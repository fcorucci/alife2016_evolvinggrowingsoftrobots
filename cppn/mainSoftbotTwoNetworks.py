#-------------------------------------------------------------------------------------------------------
# Please cite the following paper if using this code for academic purposes:
# 
# F. Corucci, N. Cheney, H. Lipson, C. Laschi, and J. Bongard, "Material properties affect evolution's ability to exploit morphological computation in growing soft-bodied creatures,"
# ALIFE XV, The Fifteenth International Conference on the Synthesis and Simulation of Living Systems, 2016
# 
# For any question, feel free to contact: f( dot )corucci( at )sssup( dot )it
#-------------------------------------------------------------------------------------------------------

import hashlib
# import matplotlib.pyplot as plt
import random
import math
import numpy as np
# from Network import *
import networkx as nx
# import Image
import scipy as sp
import scipy.signal as signal
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
import copy
import os
import time
import sys
import glob
# install scikit-image from http://scikit-image.org/docs/dev/install.html
# from skimage.morphology import skeletonize
# from Window import *
from networkx.algorithms.traversal.depth_first_search import dfs_tree
import subprocess as sub
from pprint import *
import robotSymmetriesFunctions as SymmUtils

def printLog(message):

	tstamp = time.strftime("[%Y/%m/%d-%H:%M:%S]")

	thisTime = time.time()
	secsFromLast = thisTime-printLog.lastTime
	minsFromLast = secsFromLast/60
	hoursFromLast = minsFromLast/60
	
	print(tstamp+' '+message+' \t (time from last call: %0.2fs %0.2fm %0.2fh)'%(secsFromLast,minsFromLast,hoursFromLast))
	sys.stdout.flush()
	sys.stderr.flush()
	printLog.lastTime = time.time()

# initialize logging time
printLog.lastTime = time.time()
#---------------------------------------------------------------------------------
# PARAMS
combinedStopCondition = True # true: stop simulation if sim time exceeded OR if the robot doesn't move (hardcoded)
MAX_MUTATIONS_ATTEMPTS = 1500 # was 1000
MAX_FITNESS_EVAL_TIME = 300 # max time allowed for evaluating a single individual. Rough estimation used to quit from deadlocks waiting for fitness files
equilibriumMode = 0
dtFrac = 0.5
simulationTime = 3.0
fitnessEvalInitTime = 0.5

maxGenerations = 1500
popSize=30
mutationStd = 0.5

origSizeX = 8 # 8, 10
origSizeY = 8 # = origSizeX
origSizeZ = 8 # = origSizeX
scalingFactor = 10
latticeDimension = 0.01
materialStiffness = 5e+006

# If evolving striffness distribution as well...
evolveStiffness = False
minElasticMod = 5e006
maxElasticMod = 5e008

enforcingSolidBase = False

# first experiments: 5e+006
#
# Possible tests with
# 5e+006
# 1e+007
# 5e+007
#

protectInnovatonAlong = -999 # -999 == no protection

nestedAgeInterval = -1 # -1 for no nesting


# activationFunctionNames = ["sigmoid","sin","abs","square","sqrt","edge","gradient","erosion","dilation","opening","closing","coral-RD","worm-RD","spiral-RD","zebrafish-RD","fingerprint-RD","unstable-RD"]
# activationFunctionNames = ["sigmoid","sin","abs","square","sqrt","edge","gradient","erosion","dilation","opening","closing","coral-RD","spiral-RD","unstable-RD"]#,"worm-RD","zebrafish-RD","fingerprint-RD"]
# activationFunctionNames = ["sigmoid","sin","abs","edge","gradient","erosion","dilation","opening","closing"]
# activationFunctionNames = ["sigmoid","sin","abs","coralReaction"]
activationFunctionNames = ["sigmoid","sin","abs","nAbs","square","nSquare","sqrt","nSqrt"] # put in negative versions of abs, square, ...
#activationFunctionProbs = [0.3,0.3,0.2,0.1,0.1]
filterRadius = 5

# mutationsPerStep = 1

# resetNetworkProb = 0.01
# removeNodeProb = 0.02
# removeLinkProb = 0.05
# addNodeProb = 0.03
# addLinkProb = 0.10
# mutateWeightProb = 0.7
# mutFunctProb = 0.1

# equal chance of any mutation for testing 
# resetNetworkProb = 1.0
removeNodeProb = 1.0
removeLinkProb = 1.0
addNodeProb = 1.0
addLinkProb = 1.0
mutateWeightProb = 1.0
mutFunctProb = 1.0

randomSelection = False # useful for some comparison to just select randomly ignoring the fitness

# mutationChances = [resetNetworkProb,removeNodeProb,removeLinkProb,addNodeProb,addLinkProb,mutateWeightProb,mutFunctProb]
mutationChances = [removeNodeProb,removeLinkProb,addNodeProb,addLinkProb,mutateWeightProb,mutFunctProb]
mutationChances = [float(i)/sum(mutationChances) for i in mutationChances] # (normalized)
# mutationFunctions = [resetNetwork,removeNode,removeLink,addNode,addLink,mutateWeight,mutFunct]

proportionNewlyGenerated = 0.0 # 0.1
proportionFromCrossover = 0.0 # 0.45 # *** CROSSOVER FUNCTION IS COMMENTED LATER ON ***
proportionFromMutation = 1.0 # 0.45

# params for random network creation (todo: set via brute force search)
numRandomNodes = 5
numRandomLinkAdds = 10
numRandomWeightChanges = 100
numRandomLinkRemovals = 5
numRandomActivationFunction = 100

minimumFitness = 10**(-12)
maximumFitness = 10**(12)

# environment and sources
sourcesEnabled = 1
growthAmplitude = 0.5 # voxel scaling factor due to growth: nominal size +/- growthAmplitude
sourcesTags = []
sourcesMotionTags = []
sourcesDistMul = 1.0 # multiplier of the safety distance. 1 = robot can barely touch the source in the worst case.
minTempFact = 0.1  # minimum temp fact (i.e. minimum size a voxel can get is minTempFact*nominalSize). Improves stability
maxTempFactChange = 0.00015 # 0.0005 # max change in tempFact in a dt (% of voxel dim) affects speed of the growth process. The bigger, the more impulsive
							# **** Should maxTempFactChange be related to dt?  *******

# sticky floor, to keep the robot in place (useful for preliminary tests on developmental soft robots)
stickyFloor = 1
floorEnabled = 1
floorLimited = 0 # ** EXPERIMENTAL ** -> strange things can happen when floor is limited.
# is thermal actuation enabled?
tempEnabled = 0

# ------------------------------------------------------------------------
runName = "TestRun"
testsBaseDir = "tests_data"
runDirectory = testsBaseDir+"/"+runName+"/"
batch = False;
randomSeed = 1
# maxID = 0
continuingRun = False

# -------------------------------------------------------------------------
# params for voxelyze
floorSlope = 0.0
inCage = 0
swarmClimb = 0
apertureProportion = 0.9
numCircles = 0 # 5
interCircleDist = int(origSizeX/2.0)
circleWidth = 0.06 # on a scale from 0 to 1
circleHeight = 1 # in voxels

# fitnessEvaluationTime = 2.0
fitnessEvaluationCycles = 20
#fitnessEvalInitTime = 0.3 # 0.5
if inCage:
	fitnessEvalInitTime = 0

actuationsPerSecond = 10

minPercentFull = 0.10
minPercentMuscle = 0.0 #0.05

savePareto = 0
saveVxaEvery = 100 #100 # -1 to never save
saveAllIndividualData = True
saveCPPNs = True

SelfCollisionsEnabled = True

softestMaterial = 5 # default = 10, hardest is 2 orders of magnitude stiffer

# ----------------------------------------------------------------------
# run settings
# evolvingNeuralNets = True
mutateBodyProb = 0.5  # mutateBrainProb = 1-mutateBodyProb -> now governing probability to mutate growthRate pattern or voxel structure


origFileForMutation = "none"
minVoxelPercentMutated  = 0.001 # was: 0.001 set to zero to disable check for neutral mutations. Will solve for sure the problem of getting stuck in the very first generations
minVoxelDiffForAgeReset = 0.001
seedIndividual = None


#--------------------------------------------------------------------------
# COMMAND LINE OPTIONS BELOW
#--------------------------------------------------------------------------
for i in range(len(sys.argv[1:])):
	if sys.argv[i+1] == "-r":
		if sys.argv[i+2] == 'a':
			randomSeed = int(sub.check_output("pwd").split("/")[-1].split("_")[-1])
			print "autosetting random seed to",randomSeed,"based on directory"
			
		else:
			try:
				randomSeed = int(sys.argv[i+2])
			except:
				print "ERROR: random seed value must come after \"-r\""
				exit(0)

	if sys.argv[i+1] == "-n":
		if sys.argv[i+2] == 'a':
			#runName = sub.check_output("pwd").split("/")[-2]
			# Let's append the run ID to the name
			runName = sub.check_output("pwd").split("/")[-2]+sub.check_output("pwd").split("/")[-1].split("_")[1]
			runName = runName.replace("\n", "") # just in case
			print "autosetting runName seed to",runName,"based on directory"
			if " " in runName:
				print "** ERROR ** Looks like there are spaces in runName, better remove them to avoid problems. Exit."
				exit(0)                         

                else:
			try:
				runName = sys.argv[i+2]
			except:
				print "ERROR: run name must come after \"-n\""
				exit(0)

	if sys.argv[i+1] == "-s":
		try:
			origSizeX = int(sys.argv[i+2])
			origSizeY = int(sys.argv[i+2])
			origSizeZ = int(sys.argv[i+2])
		except:
			print "ERROR: size of creature must come after \"-s\""
			exit(0)

	if sys.argv[i+1] == "-p":
		try:
			popSize = int(sys.argv[i+2])
		except:
			print "ERROR: population size of creature must come after \"-p\""
			exit(0)

	if sys.argv[i+1] == "--protect":
		try:
			protectInnovatonAlong = int(sys.argv[i+2])
			print "reset age of network", protectInnovatonAlong,"after each change"
		except:
			print "ERROR: Axis of innovation protection must come after \"--protect\""
			exit(0)

	if sys.argv[i+1] == "--nested":
		try:
			nestedAgeInterval = int(sys.argv[i+2])
			print "nested age interval set to:", nestedAgeInterval
		except:
			print "ERROR: Axis of innovation protection must come after \"--protect\""
			exit(0)

	if sys.argv[i+1] == "--mutateBodyProb":
		try:
			mutateBodyProb = sys.argv[i+2]
			if mutateBodyProb == "-a":
				# print sub.check_output("pwd")
				mutateBodyProb = float(sub.check_output("pwd").split("/")[-2].split("--mutBody_")[1])
			else:
				mutateBodyProb = float(sys.argv[i+2])
			# print "mutateBodyProb =", mutateBodyProb
		except:
			print "ERROR: mutateBodyProb must come after \"--mutateBodyProb\""
			exit(0)

	if sys.argv[i+1] == "--seedIndividual":
		try:
			seedIndividual = sys.argv[i+2]
		except:
			print "ERROR: mutateBodyProb must come after \"--mutateBodyProb\""
			exit(0)
		if not (len(glob.glob(seedIndividual[0:-5]+"0.txt")) == 1 and len(glob.glob(seedIndividual[0:-5]+"1.txt")) == 1):
			print "ERROR: seed individual not found!"
			exit(0)

	if sys.argv[i+1] == "--maxGens":
		try:
			maxGenerations = int(sys.argv[i+2])
		except:
			print "ERROR: maxGenerations must come after \"--maxGens\""
			exit(0)

	if sys.argv[i+1] == "--minDiffForReset":
		try:
			minVoxelDiffForAgeReset = float(sys.argv[i+2])
		except:
			print "ERROR: minVoxelDiffForAgeReset must come after \"--minDiffForReset\""
			exit(0)

	if sys.argv[i+1] == "--saveVxaEvery":
		try:
			saveVxaEvery = int(sys.argv[i+2])
		except:
			print "ERROR: saveVxaEvery must come after \"--saveVxaEvery\""
			exit(0)
	
	if sys.argv[i+1] == "--sourcesEnabled":
		try:
			sourcesEnabled = int(sys.argv[i+2])
		except:
			print "ERROR: sourcesEnabled must come after \"--sourcesEnabled\""
			exit(0)	

	if sys.argv[i+1] == "-t":
		try:
			tempEnabled = int(sys.argv[i+2])
		except:
			print "ERROR: tempEnabled must come after \"-t\""
			exit(0)	

	if sys.argv[i+1] == "--dtFrac":
		try:
			dtFrac = float(sys.argv[i+2])
		except:
			print "ERROR: dtFrac must come after \"--dtFrac\""
			exit(0)	


	if sys.argv[i+1] == "--simTime":
		try:
			simulationTime = float(sys.argv[i+2])
		except:
			print "ERROR: simTime must come after \"--simTime\""
			exit(0)	


	if sys.argv[i+1] == "--transientTime":
		try:
			fitnessEvalInitTime = float(sys.argv[i+2])
		except:
			print "ERROR: transientTime must come after \"--transientTime\""
			exit(0)	


	if sys.argv[i+1] == "--growthAmplitude":
		try:
			growthAmplitude = float(sys.argv[i+2])
		except:
			print "ERROR: growthAmplitude must come after \"--growthAmplitude\""
			exit(0)	


	if sys.argv[i+1] == "--latticeDim":
		try:
			latticeDimension = float(sys.argv[i+2])
		except:
			print "ERROR: latticeDim must come after \"--latticeDim\""
			exit(0)	

	if sys.argv[i+1] == "--solidBase":
		try:
			enforcingSolidBase = int(sys.argv[i+2])
		except:
			print "ERROR: solidBase must come after \"--solidBase\""
			exit(0)	

	if sys.argv[i+1] == "--stickyFloor":
		try:
			stickyFloor = int(sys.argv[i+2])
		except:
			print "ERROR: stickyFloor must come after \"--stickyFloor\""
			exit(0)	

	if sys.argv[i+1] == "--floorEnabled":
		try:
			floorEnabled = int(sys.argv[i+2])
		except:
			print "ERROR: floor must come after \"--floorEnabled\""
			exit(0)	

	if sys.argv[i+1] == "--equilibriumMode":
		try:
			equilibriumMode = int(sys.argv[i+2])
		except:
			print "ERROR: equilibriumMode must come after \"--equilibriumMode\""
			exit(0)	

	if sys.argv[i+1] == "--savePareto":
		try:
			savePareto = int(sys.argv[i+2])
		except:
			print "ERROR: savePareto must come after \"--savePareto\""
			exit(0)	

	if sys.argv[i+1] == "--minTempFact":
		try:
			minTempFact = float(sys.argv[i+2])
		except:
			print "ERROR: minTempFact must come after \"--minTempFact\""
			exit(0)	

	if sys.argv[i+1] == "--maxTempFactChange":
		try:
			maxTempFactChange = float(sys.argv[i+2])
		except:
			print "ERROR: maxTempFactChange must come after \"--maxTempFactChange\""
			exit(0)	


	if sys.argv[i+1] == "--matStiffness":
		try:
			materialStiffness = float(sys.argv[i+2])
		except:
			print "ERROR: materialStiffness must come after \"--materialStiffness\""
			exit(0)	

	if sys.argv[i+1] == "--evolveStiffness":
		try:
			tokens = sys.argv[i+2].split(',')
			if len(tokens) != 2:
				raise Exception

			minElasticMod = float(tokens[0])
			maxElasticMod = float(tokens[1])
			evolveStiffness = True
		except:
			print "ERROR: comma separated min and max elastic modulus must come after \"--evolveStiffness\""
			exit(0)	


	if sys.argv[i+1] == "--addSources":
		#
		#		     				U (up) (z axis)
		#							  .
		#		       				  .
		#		       				  .  	  B (back)
		#						  +------+   . 
		#						.'|    .'| .
		#		 			  +---+--+'  .
		#	L (left) . . . . |   |  | . |. . . . . R (right) (y axis)
		#		 			 |   +--+---+
		#                    | .'   | .' 
		#		 			 +------+'
		#			     	  .   .
		#			     	.     .
		#			      .       .
		#		  F (front) (x)    D (down)
		# FORMAT:
		# R - L 	right, left
		# U - D		up, down
		# F - B		front, back				
		# 
		# Examples: 
		# - add two equidistant sources, one on the Right, one on the Left: --addSources R,L
		# - add two equidistant sources, one in Front, one Back:            --addSources F,B
		# - add two equidistant sources, one FrontRight, one BackLeft:		--addSources FR,BL
		# - add four equidistant sources, diagonally arranged: 				--addSources FR,LB,BR,FL
		# - Is it also possible to place sources farther away by repeating a tag. E.g. place a source on the right: R. Double the distance: RR
		#  
		# ADDITION: sources motion. The tag is extended as: --addSources POSITION1["ampX freqX ampY freqY ampZ freqZ"],POSITION2[] where amp and freq are 
		# 			amplitude and frequency of the oscillation along the specified axis. 
		#
		
		try:
			
			tokens = str(sys.argv[i+2]).split(",") # let's first isolate information related to each source
			
			# now each token can be just a position specifier, e.g. RU, or a more complex one including
			# the motion parameters for this source e.g. RU[ampx freqy ampy freqy ampz freqz]
			for t in tokens:
				tt = t.split('[')
				
				if len(tt) == 1: # I'll assume this tag does not have motion specifiers
					sourcesTags.append(tt[0])
					sourcesMotionTags.append([0.0,0.0,0.0,0.0,0.0,0.0]) # no motion for this source
				elif len(tt) == 2: # I'll assume we have a compount tag including motion parameters.
					sourcesTags.append(tt[0]) # this should be the position (e.g. RL)
					tmp = tt[1] # this is e.g. "0.1 0.2 0.3]"
					tmp = tmp[0:len(tmp)-1] # let's remove the trailing ']'
					tmp = tmp.split(' ')
					if len(tmp) != 6: # we expect exactly 6 numbers, amp and freq for X, Y, Z axes
							raise Exception
					sourcesMotionTags.append(map(float, tmp)) # let's convert to a list of floats and append					
				else:
					raise Exception

		except Exception:
			print "ERROR: addSources must come after \"--addSources\".\n"\
			"Position tags: R-L (Right, Left) U-D (Up, Down), F-B (Front, Back).\n"\
			"Use commas to separe different sources. Example: --addSources FU,RFD\n"\
			"Sources motion can be specified as --addSources POSITION1[\"ampX freqX ampY freqY ampZ freqZ\"]"			
			exit(0)	

	# if sys.argv[i+1] == "--floorSize":		
	# 	try:
	# 		floorSize = map(float,sys.argv[i+2].split(","))
	# 		if len(floorSize)!=3:
	# 			print "ERROR: expecting three values after --floorSize"
	# 			exit(0)
	# 		floorLimited = True;
	# 		print "*** WARNING *** The limited floor size is experimental. At the moment it is better to use only the --floorLimited option, with default floor dimensions."
	# 	except:
	# 		print "ERROR: floorSize must come after \"--floorSize\".\n"\
	# 		"E.g. --floorSize 0.1,0.1,0.1\n"
	# 		exit(0)				

	if sys.argv[i+1] == "--floorLimited":
		try:
			floorLimited = int(sys.argv[i+2]) # will use default values in this case
			print "*** WARNING *** The limited floor size is experimental.."			
		except:
			print "ERROR: floorLimited must come after \"--floorLimited\""
			exit(0)	

	if sys.argv[i+1] == "--sourcesDistMul":
		try:
			sourcesDistMul = float(sys.argv[i+2])
		except:
			print "ERROR: sourcesDistMul must come after \"--sourcesDistMul\""
			exit(0)	

	if sys.argv[i+1] == "--batch":
		try:
			batch = int(sys.argv[i+2])
			runDirectory = ""
		except:
			print "ERROR: batch must come after \"--batch\""
			exit(0)	

	if sys.argv[i+1] == "--randomSelection":
		try:
			randomSelection = int(sys.argv[i+2])
			if randomSelection == 1:
				print "*****************************************************************"
				print "* WARNING: random selection activated! Fitness will be ignored! *"
				print "* You may want this in very few cases						   *"
				print "*****************************************************************"

		except:
			print "ERROR: randomSelection must come after \"--randomSelection\""
			exit(0)	

	if sys.argv[i+1] == "--maxMutationAttempts":
		try:
			MAX_MUTATIONS_ATTEMPTS = int(sys.argv[i+2])
		except:
			print "ERROR: maxMutationAttempts must come after \"--maxMutationAttempts\""
			exit(0)


	if sys.argv[i+1] == "--minVoxelPercentMutated":
		try:
			minVoxelPercentMutated = float(sys.argv[i+2])
		except:
			print "ERROR: minVoxelPercentMutated must come after \"--minVoxelPercentMutated\""
			exit(0)


	if sys.argv[i+1] == "--combinedStopCondition":
		try:
			combinedStopCondition = int(sys.argv[i+2])
		except:
			print "ERROR: combinedStopCondition must come after \"--combinedStopCondition\""
			exit(0)

floorRadius = 1.0*origSizeX*latticeDimension*np.sqrt(2)*0.5 # fix in case of non-square base

if not(batch):
	# Make sure that we have the latest version of voxelyze in this folder
	sub.call("cp ../voxcad-code-2014/voxelyzeMain/voxelyze .", shell=True)

if swarmClimb:
	origSizeY *= 2


if ~tempEnabled:
	minPercentMuscle = 0.0 # disable the check for admissible mutations based on the % of muscles if muscles are disabled

# if "-f" in sys.argv[1:]:
# 	# origSizeX = 200
# 	# origSizeY = 200
# 	activationFunctionNames = ["sigmoid","sin","abs","square","sqrt"]#,"edge","gradient","erosion","dilation","opening","closing"]
	
# for arg in sys.argv[1:]:
# 	if "-m" in arg:
# 		mutationsPerStep = int(arg[2:])
# 		print "mutationsPerStep =",mutationsPerStep

#for arg in sys.argv[1:]:
#	if "-c" in arg:
#		print "continueing from old run!"
#		continuingRun = True
#
#		try:
#			numCPPNFolders = len(sub.check_output("ls -d "+runDirectory+"cppn_gml/* | tail -2",shell=True).strip().split("\n"))
#		except:
#			numCPPNFolders = 0
#
#		if numCPPNFolders == 0:
#			print
#			print "ERROR: must have seed population in "+runDirectory+"cppn_gml/Gen_****.  exiting."
#			exit(0)
#		elif numCPPNFolders == 2:
#			lastGenChecked = int(sub.check_output("ls -d "+runDirectory+"cppn_gml/* | tail -1 | cut -d _ -f 3",shell=True)) - 1
#			print "starting from second to last cppn_gml gen folder, Gen:",lastGenChecked
#		elif numCPPNFolders == 1:
#			lastGenChecked = int(sub.check_output("ls -d "+runDirectory+"cppn_gml/* | tail -1 | cut -d _ -f 3",shell=True))
#			print "only one cppn_gml folder avaliable."
#			print "starting from cppn_gml gen folder, Gen:",lastGenChecked
#			if lastGenChecked == 1:
#				print "ERROR: Cannot restart at Gen 1.  exiting."

# --------------------------------------------------------------------------------------
# mutate old individual:
for i in range(len(sys.argv[1:])):
	if sys.argv[i+1] == "--mutate":
		origFileForMutation = sys.argv[i+2][0:-5]

				
#------------------------------------------------------------------------------------

# SET UP
blurMatrix = 1.0/16*np.array([[[1,2,1],
							   [2,4,2],
							   [1,2,1]]])

edgeMatrix = np.array([[[-1,-1,-1],
						[-1, 8,-1],
						[-1,-1,-1]]])

filterSize = (int(min(origSizeX,filterRadius)),int(min(origSizeY,filterRadius)),int(min(origSizeZ,filterRadius)))
#------------------------------------------------------------------------------------

#
# Shall we refactor the way we define those arrays?
#
if evolveStiffness:
	inputNodeNames = [
						['x','y','z','d','b'], # abs position (XYZ), distance from center (polar radius), bias
						['x','y','z','d','b']					 ]

	outputNodeNames = [
						["materialPresent", "stiffness"],  # CPPN1 defining static properties of the morphology
						["growthRate"] # plasticStiffness  # CPPN2 defining developmental parameters						
						
						# ["materialPresent","materialMuscleOrTissue"],#,"materialMuscleType"], #,"materialHardOrSoft"],  <-- morphology network
						# ["phaseOffset","frequency", "growthRate"] # <-- controller network
					  ]
else:
	inputNodeNames = [
						['x','y','z','d','b'],  # abs position (XYZ), distance from center (polar radius), bias
						['x','y','z','d','b']
					 ]

	outputNodeNames = [
						["materialPresent"], # CPPN1 defining static properties of the morphology
						["growthRate"]	     # CPPN2 defining developmental parameters
						
						# ["materialPresent","materialMuscleOrTissue"],#,"materialMuscleType"], #,"materialHardOrSoft"],  <-- morphology network
						# ["phaseOffset","frequency", "growthRate"] # <-- controller network
					  ]

allNetworksOutputs = []
for i in outputNodeNames:
	for k in i:
		allNetworksOutputs.append(k)

def netId(outputName):
	networkId = -1
	
	idx = 0
	for netOutputs in outputNodeNames:
		if outputName in netOutputs:
			return idx					
		idx += 1	

	return networkId

def remapStiffness(s): # gets a CPPN output in [-1,1] and remaps it into minElasticMod, maxElasticMod	
	return 	(((s+1.0)/2.0)*(maxElasticMod-minElasticMod)+minElasticMod)

def mainTwoNetworks():

	# ---------------------------------------------------------------------------
	# INITIALIZE RUN
	startAll = time.time()
	random.seed(randomSeed)
	global maxID
	maxID = 0
	global maxGenerations
	global totalEvaluations
	totalEvaluations = 0
	global gen
	gen = 0
	global alreadyEvaluated
	alreadyEvaluated = {}
	global alreadyEvaluatedShape
	alreadyEvaluatedShape = {}
	global bestDistOnlySoFar
	bestDistOnlySoFar = -99999
	global bestEnergyOnlySoFar
	bestEnergyOnlySoFar = -99999
	global bestFitOnlySoFar
	bestFitOnlySoFar = maximumFitness #-99999
	global bestObj1SoFar
	bestObj1SoFar = -99999


	sub.call("clear", shell=True)

	printRunParams()

	initializeFolders()

	# ---------------------------------------------------------------------------------
	# INIT POPULATION

	gen = 0
	totalEvaluations = 0
	alreadyEvaluated = {}
	makeGenDirs(gen)

	population = initializePopulation()

	evaluateAll(population)

	paretoSelection(population,gen) # no selection happening, only to produce dominatedBy stats to use in statFile writing (new population is never set to replace population)
	writeGenStats(population,gen)

	# ---------------------------------------------------------------------------------
	# ITERATE THROUGH EVOLUTION
	while gen < maxGenerations:
		
		gen += 1	
		printLog("Creating folders structure for this generation")
		makeGenDirs(gen)

		# ----------------------------------------------------------------------------
		# Update ages
		updateAges(population)

		# -------------------------------------------------------------------------------
		# # PERFORM CROSSOVER
		# createNewChildreThroughCrossover(population)

		# -------------------------------------------------------------------------------
		# PERFORM MUTATION
		printLog("Mutation starts")
		newChildren = createNewChildrenThroughMutation(population,newChildren=[])
		printLog("Mutation ends: successfully generated %d new children."%(len(newChildren)))
		# ------------------------------------------------------------------------------------------
		# FILL IN REST WITH NEW RANDOM INDIVIDUALS
		# newChildren = fillNewChildrenWithRandomIndividuals(population,newChildren)


		# ------------------------------------------------------------------------------
		# EVALUATE FITNESS		
		printLog("Starting fitness evaluation")
		evaluateAll(newChildren)
		printLog("Fitness evaluation finished")

		# --------------------------------------------------------------------------
		# combine children and parents for selection
		printLog("Now creating new population")
		population += newChildren
		printLog("New population size is %d"%len(population))

		# ---------------------------------------------------------------------------
		# PERFORM SELECTION BY PARETO FRONTS
		# select surviors for new population

		newPopulation = paretoSelection(population,gen)

		# --------------------------------------------------------------------------------
		# PRINT POPULATION TO STDOUT AND SAVE ALL INDIVIDUAL DATA
		printLog("Saving statistics")
		writeGenStats(population,gen)

		population = newPopulation	

	# --------------------------------------------------------------------------
	# OUTPUT END OF RUN STATS
	print 
	print "Finished "+str(maxGenerations)+ " generatons in "+str(time.time()-startAll)+" seconds ( = "+str((time.time()-startAll)/60)+" minutes = "+str((time.time()-startAll)/(3600))+" hours)"
	printLog("DONE!")
	sub.call("touch RUN_FINISHED", shell=True)
	print
	# --------------------------------------------------------------------------


def printRunParams():
	# PRINT RUN PARAMS:
	print "#####################################################################################"
	print "RUN PARAMETERS:"
	print 
	printName("maxGenerations")
	printName("popSize")
	printName("mutationStd")
	printName("origSizeX")
	printName("origSizeY")
	printName("origSizeZ")
	printName("activationFunctionNames")
	printName("scalingFactor")
	printName("filterRadius")
	printName("mutationChances")
	printName("proportionNewlyGenerated")
	printName("proportionFromCrossover")
	printName("proportionFromMutation")
	printName("numRandomNodes")
	printName("numRandomLinkAdds")
	printName("numRandomWeightChanges")
	printName("numRandomLinkRemovals")
	printName("numRandomActivationFunction")
	printName("minimumFitness")
	printName("maximumFitness")

	print
	printName("inputNodeNames")
	printName("outputNodeNames")
	printName("protectInnovatonAlong")
	printName("mutateBodyProb")	
	print
	printName("runName")
	printName("randomSeed")
	printName("continuingRun")
	printName("origFileForMutation")
	printName("minVoxelPercentMutated")
	printName("minVoxelDiffForAgeReset")
	print
	printName("floorSlope")
	printName("inCage")
	printName("swarmClimb")
	printName("apertureProportion")
	printName("fitnessEvaluationCycles")
	printName("fitnessEvalInitTime")
	printName("actuationsPerSecond")
	printName("minPercentFull")
	printName("minPercentMuscle")
	printName("saveVxaEvery")
	printName("saveAllIndividualData")
	printName("saveCPPNs")
	printName("SelfCollisionsEnabled")
	printName("softestMaterial")
	printName("sourcesEnabled")
	printName("growthAmplitude")
	printName("tempEnabled")
	printName("stickyFloor")
	printName("dtFrac")
	printName("simulationTime")
	printName("fitnessEvalInitTime")
	printName("equilibriumMode")
	printName("savePareto")
	printName("latticeDimension")
	printName("floorEnabled")
	
	printName("sourcesDistMul")
	printName("sourcesTags")
	printName("sourcesMotionTags")

	printName("maxTempFactChange")
	printName("minTempFact")
	printName("floorLimited")
	printName("floorRadius")
	printName("batch")
	printName("randomSelection")
	printName("MAX_MUTATIONS_ATTEMPTS")
	printName("minVoxelPercentMutated")
	printName("combinedStopCondition")
	printName("enforcingSolidBase")
	printName("materialStiffness")
	printName("evolveStiffness")
	printName("minElasticMod")
	printName("maxElasticMod")

	print 
	print "#####################################################################################"

def initializeFolders():

	if not(batch):
		sub.call("mkdir "+testsBaseDir+" 2>/dev/null", shell=True)
		ret = sub.call("mkdir "+runDirectory+" 2>/dev/null", shell=True)
		if ret != 0:
			response = raw_input("****************************************************\n"\
			"** WARNING ** A directory named "+runDirectory+" may exist already and would be erased.\n"\
			"ARE YOU SURE YOU WANT TO CONTINUE? (y/n): ")
			if not(("Y" in response) or ("y" in response)):
				quit("Please change run name with -n DifferentName. Quitting.\n"\
				"****************************************************\n\n")
			else:
				print "****************************************************\n"		
	
		# clear directory
		sub.call("rm -rf "+runDirectory+"*", shell=True)

	sub.call("mkdir "+runDirectory+"voxelyzeFiles 2> /dev/null",shell=True)
	sub.call("mkdir "+runDirectory+"tempFiles 2> /dev/null",shell=True)
#	sub.call("rm -f "+runDirectory+"voxelyzeFiles/*.vxa",shell=True)
	sub.call("mkdir "+runDirectory+"fitnessFiles 2> /dev/null",shell=True)
	sub.call("mkdir "+runDirectory+"bestSoFar 2> /dev/null",shell=True)
	sub.call("mkdir "+runDirectory+"bestSoFar/paretoFronts 2> /dev/null",shell=True)

	#sub.call("mkdir bestSoFar/distOnly",shell=True)
	# sub.call("mkdir bestSoFar/energyOnly",shell=True)
	sub.call("mkdir "+runDirectory+"bestSoFar/fitOnly",shell=True)


	#sub.call("rm -f "+runDirectory+"bestSoFar/*.vxa",shell=True)
	#sub.call("rm -f "+runDirectory+"bestSoFar/fitOnly/*.vxa",shell=True)
	#sub.call("rm -rf "+runDirectory+"Gen_*",shell=True)

	champFile = open(runDirectory+"bestSoFar/bestOfGen.txt",'w')
	if sourcesEnabled:
		champFile.write("gen\tid\tdistSum\t\tvoxNum\t%%exp\t\tvolStart\tchullStart\tvolEnd\t\tchullEnd\tshGlobalSymmetryIdx\tshXSymmetryIdxAggr\tshYSymmetryIdxAggr\tshZSymmetryIdxAggr\tgrGlobalSymmetryIdx\tgrXSymmetryIdxAggr\tgrYSymmetryIdxAggr\tgrZSymmetryIdxAggr\n")
	else:
		champFile.write("gen\t\tfitness\t\tdistance\t\tage\n")

	champFile.write("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n")
	champFile.close()

	if saveAllIndividualData:
		sub.call("mkdir "+runDirectory+"allIndividualsData",shell=True)
		sub.call("rm -f "+runDirectory+"allIndividualsData/*",shell=True)

	if saveCPPNs:
		sub.call("mkdir "+runDirectory+"cppn_gml",shell=True)
		sub.call("rm -rf "+runDirectory+"cppn_gml/*",shell=True)

def initializePopulation(population = []):
	global maxID

	counter = 0
	while len(population) < popSize:
		# tmpPop = []
		# for i in range(popSize-len(population)):
		individual = [createRandomNetwork(0),createRandomNetwork(1)]
		individual[0].graph["id"] = maxID
		individual[0].graph["parentID1"] = -1
		individual[0].graph["parentID2"] = -1
		individual[0].graph["parentMd5Shape1"] = "none"
		individual[0].graph["parentMd5Control1"] = "none"
		individual[0].graph["parentFrequency"] = "none"
		individual[0].graph["parentDist1"] = -1
		individual[0].graph["parentDist2"] = -1
		individual[0].graph["parentFit1"] = -1
		individual[0].graph["parentFit2"] = -1
		individual[0].graph["age"] = 0
		individual[0].graph["trueAge"] = 0
		individual[0].graph["networkNum"] = -1
		individual[0].graph["md5"] = "none"
		individual[0].graph["variationType"] = "newlyGenerated"
		individual[0].graph["voxelDiff"] = 0
		individual[0].graph["controlDiff"] = 0
		individual[0].graph["shapeDiff"] = 0
		maxID += 1

		createPhenotype(individual)
		
		shapeMatrixNew = makeShapeMaxtrix(individual)

		if np.sum(shapeMatrixNew>0) >= minPercentFull*origSizeX*origSizeY*origSizeZ and np.sum(shapeMatrixNew>2) >= minPercentMuscle*origSizeX*origSizeY*origSizeZ:
			population.append(individual)
		counter += 1

	print popSize,"viable individuals found in",counter,"attempts"
	return population

def makeShapeMaxtrix(individual):
	shapeMatrix = np.zeros((origSizeX,origSizeY,origSizeZ))
	makeOneShapeOnly(individual)
	for z in range(origSizeZ):
		for y in range(origSizeY):
			for x in range(origSizeX):
				if individual[netId("materialPresent")].node["materialPresent"]["oneShapeOnly"][x,y,z] <= 0:
					shapeMatrix[x,y,z] = 0
				elif ("materialHardOrSoft" in allNetworksOutputs) and (individual[netId("materialHardOrSoft")].node["materialHardOrSoft"]["state"][x,y,z] > 0):
					shapeMatrix[x,y,z] = 2
				elif ("materialMuscleOrTissue" not in allNetworksOutputs) or (individual[netId("materialMuscleOrTissue")].node["materialMuscleOrTissue"]["state"][x,y,z] > 0):
					# if individual[1*("materialMuscleType" in outputNodeNames[1])].node["materialMuscleType"]["state"][x,y,z] <= 0:
						shapeMatrix[x,y,z] = 3
					# else:
					# 	shapeMatrixNew[x,y,z] = 4
				else:
					shapeMatrix[x,y,z] = 1
	if enforcingSolidBase:
		# Find the first non-empty layer...	
		for z in range(origSizeZ):
			if np.sum(shapeMatrix[:,:,z]) != 0:
				#print "*****************************"
				#print(shapeMatrix[:,:,z])
				shapeMatrix[:,:,z] = 3	
				#print(shapeMatrix[:,:,z])
				#print "*****************************"		
				break
		# and fill it in. What material does not really matter, nor the adaptive properties
		# the part contacting with the ground does not change.
		# Note: this works because later on we'll shift the robot so that it touches
		# the ground. The base will touch.
	
	return shapeMatrix

def makeGrowthMatrix(individual):
	growthRateMatrix = np.zeros((origSizeX, origSizeY, origSizeZ))

	if "growthRate" in allNetworksOutputs:
		growthRateMatrix = np.zeros((origSizeX, origSizeY, origSizeZ))

		for z in range(origSizeZ):
			for y in range(origSizeY):
				for x in range(origSizeX):
						growthRateMatrix[x,y,z] = individual[netId("growthRate")].node["growthRate"]["state"][x,y,z]

	#print "-------------\nDEBUG: GrowthRateMatrix:\n-------------"
	#print "Individual: %d"%(individual[0].graph["id"])
	#print growthRateMatrix	
	return growthRateMatrix


def makeStiffnessMatrix(individual):
	stiffnessMatrix = np.zeros((origSizeX, origSizeY, origSizeZ))

	if "stiffness" in allNetworksOutputs:
		for z in range(origSizeZ):
			for y in range(origSizeY):
				for x in range(origSizeX):
						stiffnessMatrix[x,y,z] = individual[netId("stiffness")].node["stiffness"]["state"][x,y,z]

	return stiffnessMatrix


def makeGenDirs(gen):
	print "\n\n"
	print "----------------------------------"
	print "---------- GENERATION",gen,"----------"
	print "----------------------------------"
	print "\n"

	if gen%saveVxaEvery == 0 and saveVxaEvery > 0:
		sub.call("mkdir "+runDirectory+"Gen_%04i"%gen,shell=True)
	if saveCPPNs:
		sub.call("mkdir "+runDirectory+"cppn_gml/Gen_%04i"%gen,shell=True)

def writeGenStats(population,gen):

	if sourcesEnabled:
		print 
		print " ----------------------------------"
		print "| GENERATION "+str(gen)+" FINISHED - REPORT: |"
		print " ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		print "dom.\tid.\tdistSum\t\tvoxs\t%%exp\tBIs\tBIe\tsSI\tsSIx\tsSIy\tsSIz\tgSI\tgSIx\tgSIy\tgSIz\t"
	else:
		print 
		print "Gen "+str(gen)+":"
		print "dom.\t\tfitness\t\tdistance\tage\ttrue age\tvoxelDiff\tshapeDiff\tcontrolDiff\t\tfrequency\t\tid\t\tmd5Shape"
	print "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

	for individual in population:
		if sourcesEnabled:
			print str(len(individual[0].graph["dominatedBy"])),
			print "\t"+str(individual[0].graph["id"]),
			print "\t%9.5f"%(individual[0].graph["sumDistanceFromSources"]),
			print "\t%d"%(individual[0].graph["voxelNumber"]),
			print "\t%.04f"%(individual[0].graph["percentExpandingVoxels"]),
			print "\t%.04f"%(individual[0].graph["volumeStart"]/individual[0].graph["chullVolumeStart"]),		
			print "\t%.04f"%(individual[0].graph["volumeEnd"]/individual[0].graph["chullVolumeEnd"]),
			print "\t%0.4f"%(individual[0].graph["shapeGlobalSymmetryIdx"]), 
			print "\t%0.4f"%(individual[0].graph["shapeXSymmetryIdxAggregated"]),
			print "\t%0.4f"%(individual[0].graph["shapeYSymmetryIdxAggregated"]),
			print "\t%0.4f"%(individual[0].graph["shapeZSymmetryIdxAggregated"]),
			print "\t%0.4f"%(individual[0].graph["growthGlobalSymmetryIdx"]), 
			print "\t%0.4f"%(individual[0].graph["growthXSymmetryIdxAggregated"]),
			print "\t%0.4f"%(individual[0].graph["growthYSymmetryIdxAggregated"]),
			print "\t%0.4f"%(individual[0].graph["growthZSymmetryIdxAggregated"])					
			# TODO ADD GROWTH SYMMETRY INDICES HERE				
			#print "\t\t"+str(individual[0].graph["md5Shape"])
		else:
			print str(len(individual[0].graph["dominatedBy"])),
			print "\t%9.5f\t"%(individual[0].graph["fitness"]),
			print "\t%9.05f\t"%(individual[0].graph["distance"]),
			print str(individual[0].graph["age"]),
			print "\t"+str(individual[0].graph["trueAge"]),
			print "\t\t"+str(individual[0].graph["voxelDiff"]),
			print "\t\t"+str(individual[0].graph["shapeDiff"]),
			print "\t\t%9.5f"%(individual[0].graph["controlDiff"]),
			print "\t\t%9.5f"%(individual[0].graph["frequency"]),
			print "\t\t"+str(individual[0].graph["id"]),
			print "\t\t"+str(individual[0].graph["md5Shape"])
	print "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

	if sourcesEnabled:
		champFile = open(runDirectory+"bestSoFar/bestOfGen.txt",'a')
		champFile.write("%d\t%d\t%.10f\t%d\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f\t%.08f\t\t%.08f\t\t%.08f\t\t%.08f\t\t%.08f\t\t%.08f\t\t%.08f\t\t%.08f\n"%(gen, population[0][0].graph["id"], population[0][0].graph["sumDistanceFromSources"], population[0][0].graph["voxelNumber"],  population[0][0].graph["percentExpandingVoxels"], population[0][0].graph["volumeStart"], population[0][0].graph["chullVolumeStart"], population[0][0].graph["volumeEnd"], population[0][0].graph["chullVolumeEnd"], population[0][0].graph["shapeGlobalSymmetryIdx"], population[0][0].graph["shapeXSymmetryIdxAggregated"], population[0][0].graph["shapeYSymmetryIdxAggregated"], population[0][0].graph["shapeZSymmetryIdxAggregated"], population[0][0].graph["growthGlobalSymmetryIdx"], population[0][0].graph["growthXSymmetryIdxAggregated"], population[0][0].graph["growthYSymmetryIdxAggregated"], population[0][0].graph["growthZSymmetryIdxAggregated"]	))
		champFile.close()
	else:
		champFile = open(runDirectory+"bestSoFar/bestOfGen.txt",'a')
		champFile.write(str(gen)+"\t\t%.08f\t\t"%(population[0][0].graph["fitness"])+"\t\t%.08f\t\t"%(population[0][0].graph["distance"])+str(population[0][0].graph["age"])+"\t"+str(population[0][0].graph["trueAge"])+"\n")
		champFile.close()

	if saveAllIndividualData:
		writeAllIndividualsData(population)

	if gen%saveVxaEvery == 0 and saveVxaEvery > 0 and saveCPPNs:
		writeCPPNs(population)

	if gen%saveVxaEvery == 0 and saveVxaEvery > 0 and savePareto:
		saveParetoFront(population, gen)

	# now we can clear the voxelyzeFiles folder
	sub.call("rm "+runDirectory+"voxelyzeFiles/*",shell=True)

def saveParetoFront(population, gen):
	sub.call("mkdir "+runDirectory+"bestSoFar/paretoFronts/Gen_%04i"%gen,shell=True)

	# "get vxa of all individuals with min dominance number (top list)"

	ind = population[0] # first individual
	bestDominance = len(ind[0].graph["dominatedBy"])

	for individual in population:
		if len(individual[0].graph["dominatedBy"]) == bestDominance:
			sub.call("mv "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%(individual[0].graph["id"])+ " " +runDirectory+"bestSoFar/paretoFronts/Gen_%04i/"%(gen)+runName+"--Gen_%04i--distSum_%.08f--voxNum_%d--id_%05i--dom_%d.vxa"%(gen,individual[0].graph["sumDistanceFromSources"],individual[0].graph["voxelNumber"],individual[0].graph["id"],len(individual[0].graph["dominatedBy"])),shell=True)
		else:
			break
	


def paretoSelection(population,gen):
	newPopulation = []
	fitnessList = []

	if randomSelection:
		print "\n** WARNING ** Random selection taking place! You may want this in very few cases!\n"
		newPopulation = population
		random.shuffle(newPopulation)
		newPopulation = newPopulation[0:popSize]

		for individual in newPopulation:
			individual[0].graph["dominatedBy"] = []
			individual[0].graph["selected"] = 1			

		return newPopulation

	# sort newest networks to the bottom, so they have an advantage in a tie (i.e. encourage neutral mutations).  
	population.sort(reverse = False, key = lambda individual: individual[0].graph["id"])

	# CALC "DOMINATED BY" FOR PARETO OPTIMIZATION
	for individual in population:
		individual[0].graph["dominatedBy"] = []
	
	# Let's now compute dominance for each individual
	# For each individual in the population...
	for individual in population:
		G = individual[0]

		# ...we analyze all the other individuals...
		for otherIndividual in population:
			otherG = otherIndividual[0]

			# if (otherG.graph["distance"] >= G.graph["distance"] and otherG.graph["fitnessEnergy"] <= G.graph["fitnessEnergy"] and otherG.graph["age"] <= G.graph["age"])\
			# if (otherG.graph["distance"] >= G.graph["distance"] and otherG.graph["age"] <= G.graph["age"])\
			
			# ...and check whether another individual (otherG) exists that dominates the current one (G), i.e. outperformes G in all objectives 
			if sourcesEnabled:
				if (otherG.graph["sumDistanceFromSources"] <= G.graph["sumDistanceFromSources"]\
				and otherG.graph["voxelNumber"] <= otherG.graph["voxelNumber"]\
				and otherG.graph["age"] <= G.graph["age"])\
				and not (G.graph["id"] in otherG.graph["dominatedBy"])\
				and not (otherG.graph["id"] == G.graph["id"]):
					G.graph["dominatedBy"] += [otherG.graph["id"]] # list of id of individuals dominating G
			else:
				if (otherG.graph["fitness"] >= G.graph["fitness"] and otherG.graph["age"] <= G.graph["age"])\
				and not (G.graph["id"] in otherG.graph["dominatedBy"])\
				and not (otherG.graph["id"] == G.graph["id"]):
					G.graph["dominatedBy"] += [otherG.graph["id"]]

		# EXTRA PENALTY FOR DOING NOTHING OR BEING INVALID
		if sourcesEnabled:
			if G.graph["sumDistanceFromSources"] == maximumFitness:
				G.graph["dominatedBy"] += [G.graph["id"] for i in range(popSize*2)]
		else:
			if G.graph["fitness"] == minimumFitness:# or G.graph["fitnessEnergy"] == 0:
				G.graph["dominatedBy"] += [G.graph["id"] for i in range(popSize*2)]

	if sourcesEnabled:
		population.sort(reverse = True,  key = lambda individual: individual[0].graph["id"]) 						# sort id fifth
		population.sort(reverse = False, key = lambda individual: individual[0].graph["age"]) 						# sort age fourth
		population.sort(reverse = False, key = lambda individual: individual[0].graph["voxelNumber"]) 				# sort voxel number third
		population.sort(reverse = False, key = lambda individual: individual[0].graph["sumDistanceFromSources"]) 	# sort sumDistanceFromSources second	
		population.sort(reverse = False, key = lambda individual: len(individual[0].graph["dominatedBy"])) 			# sort dominated by first
	else:
		population.sort(reverse = True,  key = lambda individual: individual[0].graph["id"]) 				# sort age third
		population.sort(reverse = False, key = lambda individual: individual[0].graph["age"]) 				# sort age third
		population.sort(reverse = True,  key = lambda individual: individual[0].graph["distance"]) 		# sort distance second
		population.sort(reverse = True,  key = lambda individual: individual[0].graph["fitness"]) 		
		population.sort(reverse = False, key = lambda individual: len(individual[0].graph["dominatedBy"])) 	# sort dominated by first


	# We now divide individuals for "pareto levels" (e.g. pareto level 0: individuals that are not dominated. pareto level 1: individuals dominated one other individual, etc.)
	done = False
	paretoLevel = 0
	while not done:
		thisLevel = []
		sizeLeft = popSize - len(newPopulation)
		for individual in population:
			if len(individual[0].graph["dominatedBy"]) == paretoLevel:
				thisLevel += [individual]

		
		# Let's now add best individuals to the new population.
		# We add the best pareto levels first until it is possible to fit them in the newPopulation	
		# print sizeLeft, len(thisLevel)
		# IF WHOLE PARETO LEVEL CAN FIT, ADD IT
		if len(thisLevel) > 0:
			if sizeLeft >= len(thisLevel):
				newPopulation += thisLevel
				
			# OTHERWISE, SELECT BY SORTED RANKING (ABOVE) (TODO: SELECT TO MAXIMIZE DIVERSITY):
			else:
				# TRUNCATE SELECTION BY DISTANCE
				# newPopulation += thisLevel[0:sizeLeft]

				# RANK PROPORTIONAL SELECTION BY DISTANCE
				newPopulation += [thisLevel[0]]
				while len(newPopulation) < popSize:

					randomNum = random.random()
					for i in range(1,len(thisLevel)):
						# print "randomNum:",randomNum, "-- floor:", math.log(i)/math.log(len(thisLevel)), "-- ceil:", math.log(i+1)/math.log(len(thisLevel)),"-- alreadyIn:",thisLevel[i] in newPopulation
						if randomNum >= math.log(i)/math.log(len(thisLevel)) and randomNum < math.log(i+1)/math.log(len(thisLevel)) and not thisLevel[i] in newPopulation:
							# print "adding individual rank",i
							newPopulation += [thisLevel[i]]
							continue

		paretoLevel += 1		
		if len(newPopulation) == popSize:
			done = True

	for individual in population:
		if individual in newPopulation:
			individual[0].graph["selected"] = 1
		else:
			individual[0].graph["selected"] = 0

	return newPopulation



def writeCPPNs(population):
	for individual in population:
		tmpInd = copy.deepcopy(individual)
		for networkNum in range(2):

			# REMOVE STATE INFORMATION TO REDUCE FILE SIZE
			for nodeName in tmpInd[networkNum].nodes():
				tmpInd[networkNum].node[nodeName]["state"] = ""
				tmpInd[networkNum].node[nodeName]["evaluated"] = 0
				if "oneShapeOnly" in tmpInd[networkNum].node[nodeName]:
					tmpInd[networkNum].node[nodeName]["oneShapeOnly"] = ""
			if "dominatedBy" in tmpInd[networkNum].graph:
				tmpInd[networkNum].graph["dominatedBy"] = ""
			if "materialDistribution" in tmpInd[networkNum].graph:
				tmpInd[networkNum].graph["materialDistribution"] = ""
			
			if sourcesEnabled:
				nx.write_gml(tmpInd[networkNum],runDirectory+"cppn_gml/Gen_%04i/cppn--id_%05i--distSum_%.08f--voxNum_%07i--net"%(gen,tmpInd[0].graph["id"],tmpInd[0].graph["sumDistanceFromSources"],tmpInd[0].graph["voxelNumber"])+str(networkNum)+".txt")			
			else:
				nx.write_gml(tmpInd[networkNum],runDirectory+"cppn_gml/Gen_%04i/cppn--fit_%.08f--dist_%.08f--id_%05i--net"%(gen,tmpInd[0].graph["fitness"],tmpInd[0].graph["distance"],tmpInd[0].graph["id"])+str(networkNum)+".txt")


def writeAllIndividualsData(population):
	recordingFile = open(runDirectory+"allIndividualsData/Gen_%04i.txt"%gen,"w")

	if sourcesEnabled:
		recordingFile.write("id\tdom\tdistSum\t\tvoxNum\t%%expanding\tvolStart\tchullStart\tvolEnd\t\tchullEnd\tshGlobalSymmetryIdx\tshXSymmetryIdxAggr\tshYSymmetryIdxAggr\tshZSymmetryIdxAggr\tgrGlobalSymmetryIdx\tgrXSymmetryIdxAggr\tgrYSymmetryIdxAggr\tgrZSymmetryIdxAggr\tenergy\t\tfrequency\tage\ttrueAge\tvoxelDiff\tShapeDiff\tcontrolDiff\tempty\ttissue\tbone\tmuscle1\tmuscle2\tnodes\tedges\tP1_id\tP2_id\tP1_fit\t\tP2_fit\t\tP1_dist\t\tP2_dist\t\tvariationType\tnetNum\tmd5\t\t\tmd5Shape\t\t\tP1_md5Shape\t\t\tmd5Control\t\t\tP1_md5Control\t\t\tP1_freq\t\tselected\n")
		for individual in population:
			G = individual[0]
			recordingFile.write(str(individual[0].graph["id"])+
								"\t"+str(len(individual[0].graph["dominatedBy"]))+
								"\t%.08f"%(individual[0].graph["sumDistanceFromSources"])+
								"\t%d"%(individual[0].graph["voxelNumber"])+
								"\t%.10f"%(individual[0].graph["percentExpandingVoxels"])+						
								"\t%.10f"%(individual[0].graph["volumeStart"])+
								"\t%.10f"%(individual[0].graph["chullVolumeStart"])+
								"\t%.10f"%(individual[0].graph["volumeEnd"])+
								"\t%.10f"%(individual[0].graph["chullVolumeEnd"])+
								"\t%.10f"%(individual[0].graph["shapeGlobalSymmetryIdx"])+
								"\t\t%.08f"%(individual[0].graph["shapeXSymmetryIdxAggregated"])+
								"\t\t%.08f"%(individual[0].graph["shapeYSymmetryIdxAggregated"])+
								"\t\t%.08f"%(individual[0].graph["shapeZSymmetryIdxAggregated"])+
								"\t\t%.10f"%(individual[0].graph["growthGlobalSymmetryIdx"])+
								"\t\t%.08f"%(individual[0].graph["growthXSymmetryIdxAggregated"])+
								"\t\t%.08f"%(individual[0].graph["growthYSymmetryIdxAggregated"])+
								"\t\t%.08f"%(individual[0].graph["growthZSymmetryIdxAggregated"])+
								"\t\t%.08f"%(individual[0].graph["energy"])+
								"\t%.08f"%(individual[0].graph["frequency"])+
								"\t"+str(individual[0].graph["age"])+
								"\t"+str(individual[0].graph["trueAge"])+
								"\t"+str(individual[0].graph["voxelDiff"])+
								"\t"+str(individual[0].graph["shapeDiff"])+
								"\t"+str(individual[0].graph["controlDiff"])+
								"\t%4i"%(individual[0].graph["materialDistribution"][0])+
								"\t%4i"%(individual[0].graph["materialDistribution"][1])+
								"\t%4i"%(individual[0].graph["materialDistribution"][2])+
								"\t%4i"%(individual[0].graph["materialDistribution"][3])+
								"\t%4i"%(individual[0].graph["materialDistribution"][4])+
								"\t"+str(len(individual[0].nodes()))+
								"\t"+str(len(individual[0].edges()))+
								"\t"+str(individual[0].graph["parentID1"])+
								"\t\t"+str(individual[0].graph["parentID2"])+
								"\t\t%.08f"%(individual[0].graph["parentFit1"])+
								"\t%.08f"%(individual[0].graph["parentFit2"])+
								"\t\t%.08f"%(individual[0].graph["parentDist1"])+
								"\t%.08f"%(individual[0].graph["parentDist2"])+
								"\t"+individual[0].graph["variationType"]+
								"\t"+str(individual[0].graph["networkNum"])+
								"\t"+individual[0].graph["md5"]+
								"\t"+individual[0].graph["md5Shape"]+
								"\t"+individual[0].graph["parentMd5Shape1"]+
								"\t"+individual[0].graph["md5Control"]+
								"\t"+individual[0].graph["parentMd5Control1"]+
								"\t"+str(individual[0].graph["parentFrequency"])+
								"\t"+str(individual[0].graph["selected"])+
								"\n")		
	else:
		recordingFile.write("id\tdom\tfitness\t\tdistance\t\tenergy\t\tfrequency\t\tage\ttrueAge\tvoxelDiff\tShapeDiff\tcontrolDiff\tempty\ttissue\tbone\tmuscle1\tmuscle2\tnodes\tedges\tP1_id\tP2_id\tP1_fit\t\tP2_fit\t\tP1_dist\t\tP2_dist\t\tvariationType\tnetNum\tmd5\t\t\tmd5Shape\t\t\tP1_md5Shape\t\t\tmd5Control\t\t\tP1_md5Control\t\t\tP1_freq\t\tselected\n")
		for individual in population:
			G = individual[0]
			recordingFile.write(str(individual[0].graph["id"])+
								"\t"+str(len(individual[0].graph["dominatedBy"]))+
								"\t%.08f"%(individual[0].graph["fitness"])+
								"\t%.08f"%(individual[0].graph["distance"])+
								"\t%.08f"%(individual[0].graph["energy"])+
								"\t%.08f"%(individual[0].graph["frequency"])+
								"\t"+str(individual[0].graph["age"])+
								"\t"+str(individual[0].graph["trueAge"])+
								"\t"+str(individual[0].graph["voxelDiff"])+
								"\t"+str(individual[0].graph["shapeDiff"])+
								"\t"+str(individual[0].graph["controlDiff"])+
								"\t%4i"%(individual[0].graph["materialDistribution"][0])+
								"\t%4i"%(individual[0].graph["materialDistribution"][1])+
								"\t%4i"%(individual[0].graph["materialDistribution"][2])+
								"\t%4i"%(individual[0].graph["materialDistribution"][3])+
								"\t%4i"%(individual[0].graph["materialDistribution"][4])+
								"\t"+str(len(individual[0].nodes()))+
								"\t"+str(len(individual[0].edges()))+
								"\t"+str(individual[0].graph["parentID1"])+
								"\t\t"+str(individual[0].graph["parentID2"])+
								"\t\t%.08f"%(individual[0].graph["parentFit1"])+
								"\t%.08f"%(individual[0].graph["parentFit2"])+
								"\t\t%.08f"%(individual[0].graph["parentDist1"])+
								"\t%.08f"%(individual[0].graph["parentDist2"])+
								"\t"+individual[0].graph["variationType"]+
								"\t"+str(individual[0].graph["networkNum"])+
								"\t"+individual[0].graph["md5"]+
								"\t"+individual[0].graph["md5Shape"]+
								"\t"+individual[0].graph["parentMd5Shape1"]+
								"\t"+individual[0].graph["md5Control"]+
								"\t"+individual[0].graph["parentMd5Control1"]+
								"\t"+str(individual[0].graph["parentFrequency"])+
								"\t"+str(individual[0].graph["selected"])+
								"\n")

def updateAges(population):
	for individual in population:
		if sourcesEnabled:
			individual[0].graph["parentDist1"] = individual[0].graph["sumDistanceFromSources"]
			individual[0].graph["parentFit1"] = individual[0].graph["voxelNumber"]
		else:
			individual[0].graph["parentDist1"] = individual[0].graph["distance"]
			individual[0].graph["parentFit1"] = individual[0].graph["fitness"]

		individual[0].graph["age"]+=1
		individual[0].graph["trueAge"]+=1
		individual[0].graph["variationType"] = "survived"
		individual[0].graph["parentDist2"] = -1
		individual[0].graph["parentFit2"] = -1
		individual[0].graph["parentID1"] = individual[0].graph["id"]
		individual[0].graph["parentMd5Shape1"] = individual[0].graph["md5Shape"]
		individual[0].graph["parentMd5Control1"] = individual[0].graph["md5Control"]
		individual[0].graph["parentFrequency"] = individual[0].graph["frequency"]
		individual[0].graph["parentID2"] = -1
		individual[0].graph["networkNum"] = -1
		individual[0].graph["voxelDiff"] = 0
		individual[0].graph["shapeDiff"] = 0
		individual[0].graph["controlDiff"] = 0

def fillNewChildrenWithRandomIndividuals(population,newChildren=[]):
	while len(newChildren) < popSize:
		individual = [createRandomNetwork(0),createRandomNetwork(1)]
		individual[0].graph["id"] = maxID
		individual[0].graph["parentID1"] = -1
		individual[0].graph["parentMd5Shape1"] = "none"
		individual[0].graph["parentID2"] = -1
		individual[0].graph["parentDist1"] = -1
		individual[0].graph["parentDist2"] = -1
		individual[0].graph["parentFit1"] = -1
		individual[0].graph["parentFit2"] = -1
		individual[0].graph["age"] = 0
		individual[0].graph["trueAge"] = 0	
		individual[0].graph["networkNum"] = -1
		individual[0].graph["md5"] = "none"
		individual[0].graph["variationType"] = "newlyGenerated"
		maxID += 1
		createPhenotype(individual)
		newChildren.append(individual)


def createNewChildreThroughCrossover(population, newChildren=[]):
	newChildren = []
	spotsToFill = popSize # - len(population)


	# PERFORM CROSSOVER
	while len(newChildren)+1 < spotsToFill*(proportionFromCrossover) and len(population) > 1:

		networkNum = 1*(random.random() > mutateBodyProb)

		random.shuffle(population)
		resultsFromCross = crossoverBoth(population[0],population[1],networkNum)

		# for i in range(2):
		for individual in resultsFromCross:
			individual[0].graph["networkNum"] = networkNum
			# if networkNum == protectInnovatonAlong: resultsFromCross[i][0].graph["age"] = 0

		for individual in resultsFromCross:
			pruneNetwork(individual,networkNum)
			createPhenotype(individual)
			newChildren.append(individual)


def createNewChildrenThroughMutation(population, newChildren=[]):
	global maxID

	# mutationFunctions = [resetNetwork,removeNode,removeLink,addNode,addLink,mutateWeight,mutFunct]
	mutationFunctions = [removeNode,removeLink,addNode,addLink,mutateWeight,mutFunct]

	# initialize these outside of function call if adding crossover or new alps individuals
	# newChildren = []
	spotsToFill = popSize

	random.shuffle(population)
	indCounter = 0
	while len(newChildren) < spotsToFill*(proportionFromMutation+proportionFromCrossover) and indCounter < len(population):
		# random.shuffle(population)

		# individual = copy.deepcopy(population[0])

		individual = copy.deepcopy(population[indCounter]) # nac: for debug nested mutation
		indCounter += 1

		networkNum = 1*(random.random() > mutateBodyProb)

		if nestedAgeInterval > 0:
			# if gen%nestedAgeInterval==0:
			if gen%nestedAgeInterval==1:
				networkNum = 0
			else:
				networkNum = 1

		individual[0].graph["networkNum"] = networkNum

		printLog("newChildrenSoFar %5d \t maxSpotsToFill: %5d \t mutatingInd: %5d / %5d \t mutatingNetNo: %d"%(len(newChildren), spotsToFill*(proportionFromMutation+proportionFromCrossover), (indCounter-1), len(population), networkNum))
		#print "Mutation %d/%d - Mutating CPPN %d"%(indCounter, len(population), networkNum)
		# if networkNum == protectInnovatonAlong:	individual[0].graph["age"] = 0


		if sourcesEnabled:
			individual[0].graph["parentDist1"] = individual[0].graph["sumDistanceFromSources"]
			individual[0].graph["parentFit1"] = individual[0].graph["voxelNumber"]
		else:
			individual[0].graph["parentDist1"] = individual[0].graph["distance"]
			individual[0].graph["parentFit1"] = individual[0].graph["fitness"]
		
		individual[0].graph["parentDist2"] = -1
		individual[0].graph["parentFit2"] = -1
		individual[0].graph["parentID1"] = individual[0].graph["id"]
		individual[0].graph["parentMd5Shape1"] = individual[0].graph["md5Shape"]
		individual[0].graph["parentMd5Control1"] = individual[0].graph["md5Control"]
		individual[0].graph["parentFrequency"] = individual[0].graph["frequency"]
		individual[0].graph["parentID2"] = -1
		individual[0].graph["id"] = maxID
		maxID += 1

		for thisNet in range(len(outputNodeNames)):
			for outputNode in outputNodeNames[thisNet]:
				individual[thisNet].node[outputNode]["oldState"] = individual[thisNet].node[outputNode]["state"]
		oldFrequency = individual[0].graph["frequency"]
		
		shapeMatrixOld = makeShapeMaxtrix(individual)

		oldIndividual = copy.deepcopy(individual)
		done = False
		mutationCounter = 0
		while not done:			

			if mutationCounter > MAX_MUTATIONS_ATTEMPTS:
				#individual = copy.deepcopy(oldIndividual)
				#shapeMatrixNew = shapeMatrixOld				
				done = True
				break
				# previously we were then adding this badly mutated individual. Now it is avoided
				# by later checking if mutatioCounter exceeded, and not adding this genome (drawbacks?)
				# another thing we could do is to add the original individual, not mutated. Something like this:
				#individual = copy.deepcopy(oldIndividual) # don't mutate.
				#shapeMatrixNew = shapeMatrixOld
	
			if ((mutationCounter+1)%500 == 0):
				print("mutationAttempt: %d"%(mutationCounter+1))			

			mutationCounter+=1
			individual = copy.deepcopy(oldIndividual)
			randomNum = random.random()
			randomProbSum = 0

			for i in range(len(mutationChances)):
				randomProbSum += mutationChances[i]
				if randomNum < randomProbSum:
					variationDegree = mutationFunctions[i](individual[networkNum],networkNum)
					# print mutationFunctions[i].__name__,"on network",networkNum,"of id",individual[0].graph["id"]
					individual[0].graph["variationType"] = mutationFunctions[i].__name__ + variationDegree
					break

			pruneNetwork(individual,networkNum)
			createPhenotype(individual)

			shapeMatrixNew = makeShapeMaxtrix(individual)


			if not(np.sum(shapeMatrixNew>0) >= minPercentFull*origSizeX*origSizeY*origSizeZ and np.sum(shapeMatrixNew==3) >= minPercentMuscle*origSizeX*origSizeY*origSizeZ):
				done = False # (just to be sure)				
				continue # this one is not admissible, no matter what. Skip.NOTE: this was causing a bug when the check for MAX_MUTATIONS_ATTEMPTS was after this block!

			if np.sum(1*((shapeMatrixNew-shapeMatrixOld)!=0)) >= minVoxelPercentMutated*np.sum(shapeMatrixOld>0):
				done = True	
				break			

			# just make sure that mutation has occurred and change the phenotype
			for outputNode in outputNodeNames[networkNum]:
				if outputNode in  ["phaseOffset"]:
					if np.sum(individual[networkNum].node[outputNode]["oldState"][shapeMatrixNew==3] != individual[networkNum].node[outputNode]["state"][shapeMatrixNew==3])>0:
						done = True
						break
				elif outputNode in  ["frequency"]:
					if abs(oldFrequency - individual[0].graph["frequency"]) > 0:
						done = True
						break
				elif outputNode in  ["growthRate", "stiffness"]:
					if np.sum(individual[networkNum].node[outputNode]["oldState"] != individual[networkNum].node[outputNode]["state"])>0:
						done = True							
						break
			# for outputNode in outputNodeNames[networkNum]:
			# 	if outputNode == "phaseOffset":
			# 		if individual[networkNum].node[outputNode]["oldStatePostHocMutation"][individual[1*("materialPresent" in outputNodeNames[1])].node["materialPresent"]["state"]]>0] != individual[networkNum].node[outputNode]["state"][individual[1*("materialPresent" in outputNodeNames[1])].node["materialPresent"]["state"]]>0]:
			# 			done = True
			# 	else:
			# 		if np.sum(1*((shapeMatrixNew-shapeMatrixOld)!=0)) >= minVoxelPercentMutated*np.sum(oldMaterialDistribution[1:]) and np.sum(materialCounts[1:]) >= minPercentFull*origSizeX*origSizeY*origSizeZ and np.sum(materialCounts[3:]) >= minPercentMuscle*origSizeX*origSizeY*origSizeZ:
			# 			done = True
		

		if mutationCounter > MAX_MUTATIONS_ATTEMPTS:
			printLog("Couldn't find a successful mutation in %d attempts! Skipping this individual. "%(MAX_MUTATIONS_ATTEMPTS))
			continue # don't add this badly mutated individual (nor add the father)		

		individual[0].graph["voxelDiff"] = np.sum(1*((shapeMatrixNew-shapeMatrixOld)!=0))
		individual[0].graph["shapeDiff"] = np.sum(1*((1*(shapeMatrixNew>0)-1*(shapeMatrixOld>0))!=0))	
		individual[0].graph["controlDiff"] = 0
		if "phaseOffset" in allNetworksOutputs:
			individual[0].graph["controlDiff"] += np.sum(np.abs( individual[1].node["phaseOffset"]["state"][shapeMatrixNew>2] - individual[1].node["phaseOffset"]["oldState"][shapeMatrixNew>2] ))
		if "frequency" in allNetworksOutputs:
			individual[0].graph["controlDiff"] += np.abs( abs(oldFrequency - individual[0].graph["frequency"]) )
		if "growthRate" in allNetworksOutputs: # controlDiff is not really used at the moment.
			individual[0].graph["controlDiff"] += np.sum(np.abs( individual[1].node["growthRate"]["state"][shapeMatrixNew>2] - individual[1].node["growthRate"]["oldState"][shapeMatrixNew>2] )	)

		# individual[0].graph["controlDiff"] = np.sum( np.abs( individual[1].node["phaseOffset"]["state"][shapeMatrixNew>2] - individual[1].node["phaseOffset"]["oldState"][shapeMatrixNew>2] ) ) + (origSizeX*origSizeY*origSizeZ)* abs(oldFrequency - individual[0].graph["frequency"]) 
											 # np.sum( np.abs( individual[1].node["frequency"]["state"][shapeMatrixNew>2] - individual[1].node["frequency"]["oldState"][shapeMatrixNew>2] ) )
		# individual[0].graph["energy"] = float(np.sum(shapeMatrixNew>2))/np.sum(shapeMatrixNew>0)			
		# if individual[0].graph["shapeDiff"] > 0:
		# 	print "shapeMatixOld:"
		# 	print 1*(shapeMatrixOld>0)
		# 	print 
		# 	print "shapeMatixNew:"
		# 	print 1*(shapeMatrixNew>0)

		# for outputNode in outputNodeNames[networkNum]:
		# 	individual[networkNum].node[outputNode]["oldState"] = None
		for thisNet in range(len(outputNodeNames)):
			for outputNode in outputNodeNames[thisNet]:
				individual[thisNet].node[outputNode]["oldState"] = ""

		newChildren.append(individual)

	return newChildren



def evaluateAll(subPopulation,numEvaluatedThisGen=0):
	global gen
	global totalEvaluations
	global bestDistOnlySoFar
	global bestEnergyOnlySoFar
	global bestFitOnlySoFar
	global bestObj1SoFar

	while True: # This outer loop allows to repeat a whole generation after checkpointing and avoid altering the run due to time out or expired threads
		numEvaluatedThisGen = 0
		restartTheWholeEvaluation = False

		if len(subPopulation) == 0:
			printLog('** WARNING ** evaluateAll called with empty population. Look like there are no new individuals to evaluate. Maybe there are problems with genetic operators failing to produce admissible individuals, or with the function in charge of inizializing the population!')
			return

		startTime = time.time()

		for individual in subPopulation: 				

			if sourcesEnabled:
				individual[0].graph["voxelNumber"] = maximumFitness
				individual[0].graph["sumDistanceFromSources"] = maximumFitness
				individual[0].graph["shapeGlobalSymmetryIdx"] 	  = -1
				individual[0].graph["shapeXSymmetryIdxAggregated"] = -1	
				individual[0].graph["shapeYSymmetryIdxAggregated"] = -1	
				individual[0].graph["shapeZSymmetryIdxAggregated"] = -1

				individual[0].graph["growthGlobalSymmetryIdx"] 	  = -1
				individual[0].graph["growthXSymmetryIdxAggregated"] = -1	
				individual[0].graph["growthYSymmetryIdxAggregated"] = -1	
				individual[0].graph["growthZSymmetryIdxAggregated"] = -1

				individual[0].graph["percentExpandingVoxels"] = -1

				individual[0].graph["volumeStart"] = -1
				individual[0].graph["volumeEnd"] = -1
				individual[0].graph["chullVolumeStart"] = -1
				individual[0].graph["chullVolumeEnd"] = -1
							
			else:
				individual[0].graph["distance"] = -999
				individual[0].graph["fitness"] = -999

			individual[0].graph["energy"] = -999

		numEvaluatedThisGen = 0

		for individual in subPopulation:
			[materialCounts,md5,md5Shape,md5Control] = writeVoxelyzeFileOrig(individual)

			# SET AGE TO ZERO FOR MORPHOLOGY PROTECTION ONLY IF MORPHOLOGY ACTUALLY CHANGED
			if individual[0].graph["networkNum"] == protectInnovatonAlong:
				if protectInnovatonAlong == 0:
					if md5Shape not in alreadyEvaluatedShape: 
						# print "RESETTING AGE OF INDIVIDUAL",individual[0].graph["id"],"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
						if individual[0].graph["voxelDiff"] >= minVoxelDiffForAgeReset*origSizeX*origSizeY*origSizeZ:
							individual[0].graph["age"] = 0
					else:
						if alreadyEvaluatedShape[md5Shape] > individual[0].graph["age"]:
							individual[0].graph["age"] = alreadyEvaluatedShape[md5Shape]
				else:
					individual[0].graph["age"] = 0

			alreadyEvaluatedShape[md5Shape] = individual[0].graph["age"]

			individual[0].graph["md5Control"] = md5Control
			individual[0].graph["md5Shape"] = md5Shape
			individual[0].graph["md5"] = md5
			individual[0].graph["materialDistribution"] = materialCounts

			# DON'T EVALUATE IF HAS NO VOXELS OR NO MUSCLES --> FC now that we have growth, it's no longer the case to do what follows, commented
	#		if sum(materialCounts[1:]) < minPercentFull*origSizeX*origSizeY*origSizeZ or sum(materialCounts[3:]) < minPercentMuscle*origSizeX*origSizeY*origSizeZ:
	#			# print "not enough voxels filled:  assigning fitness of 10^-12 \n"
	#			individual[0].graph["distance"] = 0.0
	#			individual[0].graph["fitness"] = minimumFitness
	#			individual[0].graph["energy"] = 0.0
	#			# print "NOT ENOUGH VOXELS OR MUSCLES, assigning minimum fitness"
	#			if gen%saveVxaEvery == 0 and saveVxaEvery > 0:
	#				sub.call("mv voxelyzeFiles/voxelyzeFile--id_%05i.vxa"%individual[0].graph["id"]+" Gen_%04i/"%(gen)+runName+"--Gen_%04i--fit_%.08f--dist_%.08f--id_%05i.vxa"%(gen,individual[0].graph["fitness"],individual[0].graph["distance"],individual[0].graph["id"]),shell=True)
	#			else:
	#				sub.call("rm voxelyzeFiles/voxelyzeFile--id_%05i.vxa"%individual[0].graph["id"],shell=True)
			
			# DON'T EVALUATE IF YOU'VE ALREADY EVALUATED THE SAME PHENOTYPE BEFORE
			if md5 in alreadyEvaluated: 
				
				if sourcesEnabled:
					individual[0].graph["voxelNumber"] = alreadyEvaluated[md5][0]
					individual[0].graph["sumDistanceFromSources"] = alreadyEvaluated[md5][1]
					individual[0].graph["energy"] = alreadyEvaluated[md5][2]
					individual[0].graph["shapeGlobalSymmetryIdx"] 	  = alreadyEvaluated[md5][3] 		
					individual[0].graph["shapeXSymmetryIdxAggregated"] = alreadyEvaluated[md5][4]	
					individual[0].graph["shapeYSymmetryIdxAggregated"] = alreadyEvaluated[md5][5]	
					individual[0].graph["shapeZSymmetryIdxAggregated"] = alreadyEvaluated[md5][6]	
					individual[0].graph["growthGlobalSymmetryIdx"] 	  = alreadyEvaluated[md5][7]
					individual[0].graph["growthXSymmetryIdxAggregated"] = alreadyEvaluated[md5][8]
					individual[0].graph["growthYSymmetryIdxAggregated"] = alreadyEvaluated[md5][9]
					individual[0].graph["growthZSymmetryIdxAggregated"] = alreadyEvaluated[md5][10]
					individual[0].graph["percentExpandingVoxels"] = alreadyEvaluated[md5][11]

					#individual[0].graph["xyzSymmetryIndices"] 	  =  		

					print "Individual ",individual[0].graph["id"]," already evaluated: cached fitness is sumDistanceFromSources = ",individual[0].graph["sumDistanceFromSources"]," voxelNumber = ",individual[0].graph["voxelNumber"]

				else:
					individual[0].graph["distance"] = alreadyEvaluated[md5][0]
					individual[0].graph["fitness"] = alreadyEvaluated[md5][1]
					individual[0].graph["energy"] = alreadyEvaluated[md5][2]
					print "Individual already evaluated:  cached fitness is",individual[0].graph["fitness"]," (",individual[0].graph["distance"],")"


				if gen%saveVxaEvery == 0 and saveVxaEvery > 0:
					if sourcesEnabled:
						sub.call("cp "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"]+" "+runDirectory+"Gen_%04i/"%(gen)+runName+"--Gen_%04i--distSum_%.08f--voxNum_%d--id_%05i.vxa"%(gen,individual[0].graph["sumDistanceFromSources"],individual[0].graph["voxelNumber"],individual[0].graph["id"]),shell=True)
					else:
						sub.call("cp "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"]+" "+runDirectory+"Gen_%04i/"%(gen)+runName+"--Gen_%04i--fit_%.08f--dist_%.08f--id_%05i.vxa"%(gen,individual[0].graph["fitness"],individual[0].graph["distance"],individual[0].graph["id"]),shell=True)
				#else:
				#	sub.call("rm "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"],shell=True)

			# EVALUATE WITH VOXELYZE
			else:
				shapeMatrixNew = makeShapeMaxtrix(individual)
				growthMatrixNew = makeGrowthMatrix(individual)

				# ---- Save symmetry indices ----------------------------------------------------------------------------------------------------------------------------------------------------
				# This can be already evaluated since it is computed on the static shape. Doesn't require to run voxelyze
				# Computing symmetry indices on the shape matrix (materials distribution) 
				(shapeGlobalSymmetryIdx, shapeXSymmetryIdxAggregated, shapeYSymmetryIdxAggregated, shapeZSymmetryIdxAggregated, shapeXyzSymmetryIndices) = SymmUtils.computeRobotSymmetryIndices(shapeMatrixNew)
				individual[0].graph["shapeGlobalSymmetryIdx"] 	  = shapeGlobalSymmetryIdx		# single number, global symmetry idx considering all three scan directions
				individual[0].graph["shapeXSymmetryIdxAggregated"] = shapeXSymmetryIdxAggregated	# single number, symmetry idx scanning the robot along x (front view), mean of the six symmetry indices defined for the six types of symmetries being analyzed (VertRefl | VerTran | HorRefl | HorTran | diagRefl | antiDiagRefl)
				individual[0].graph["shapeYSymmetryIdxAggregated"] = shapeYSymmetryIdxAggregated	# same as above, scanning along y (lateral view)
				individual[0].graph["shapeZSymmetryIdxAggregated"] = shapeZSymmetryIdxAggregated	# same as above, scanning along z (top view)
				#individual[0].graph["xyzSymmetryIndices"] 	  = xyzSymmetryIndices 		# Can't store it with current data structure for alreadyEvaluated. Ignoring... | A 6-components vector, each row contains the 6 symmetry descriptors being computed when scanning along a given direction (x, y, z)
				# Computing symmetry indices on the growthRate matrix (float matrix)
				(growthGlobalSymmetryIdx, growthXSymmetryIdxAggregated, growthYSymmetryIdxAggregated, growthZSymmetryIdxAggregated, growthXyzSymmetryIndices) = SymmUtils.computeRobotSymmetryIndices(growthMatrixNew)
				individual[0].graph["growthGlobalSymmetryIdx"] 	  = growthGlobalSymmetryIdx		# single number, global symmetry idx considering all three scan directions
				individual[0].graph["growthXSymmetryIdxAggregated"] = growthXSymmetryIdxAggregated	# single number, symmetry idx scanning the robot along x (front view), mean of the six symmetry indices defined for the six types of symmetries being analyzed (VertRefl | VerTran | HorRefl | HorTran | diagRefl | antiDiagRefl)
				individual[0].graph["growthYSymmetryIdxAggregated"] = growthYSymmetryIdxAggregated	# same as above, scanning along y (lateral view)
				individual[0].graph["growthZSymmetryIdxAggregated"] = growthZSymmetryIdxAggregated	# same as above, scanning along z (top view)

				percentExpandingVoxels = float(np.sum(growthMatrixNew > 0))/(origSizeX*origSizeY*origSizeZ)
				individual[0].graph["percentExpandingVoxels"] = percentExpandingVoxels
				# -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				# individual[0].graph["energy"] = float(np.sum(shapeMatrixNew>2))/(origSizeX*origSizeY*origSizeZ)#/np.sum(shapeMatrixNew>0)
				individual[0].graph["energy"] = float(np.sum(shapeMatrixNew>0))/(origSizeX*origSizeY*origSizeZ)#/np.sum(shapeMatrixNew>0)

				numEvaluatedThisGen += 1
				totalEvaluations += 1
				# print "Staring Voxelyze..."
				sub.Popen("./voxelyze --estimateVolumes -f "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"],shell=True) # TODO handle fails (e.g. what happens if a voxelyze istance crashes?)

		print "\nLaunched",numEvaluatedThisGen,"voxelyze calls, out of",len(subPopulation),"individuals"

				
		numEvalsFinished = 0
		allDone = False
		alreadyAnalyzedIds = []
		bestOfGenFilename = "";
		newBestFound = False

		fitnessEvalStartTime = time.time()

		while not allDone:
			
			timeWaitingForFitness = time.time() - fitnessEvalStartTime # This should protect us from getting stuck when, for some reason, voxelyze doesn't return a fitness value (diverging simulations, crash, error reading .vxa)
			if timeWaitingForFitness > popSize * MAX_FITNESS_EVAL_TIME: # WARNING: This could in fact alter the simulation and undermine the reproducibility. Maye better to come up with an alternative strategy.
				
				if os.path.isfile(runDirectory+"RESUMED"): # Simulation was checkpointed in the middle of evaluation, let's reset the counter and proceed
					#printLog("Looks like the evaluation function was interrupted for checkpointing. Better restart this evaluation to avoid mess with checkpointing.")
					restartTheWholeEvaluation = True
					# Reset counter
					fitnessEvalStartTime = time.time()
					sub.call("rm "+runDirectory+"RESUMED",shell=True) # removing the file, no excuses next time!
					allDone = False
					break	
				else:
					allDone = False # something bad with this individual, probably simulation diverged
					break

			# CHECK TO SEE IF ALL ARE FINISHED:
			allDone = True

			for individual in subPopulation:

				if sourcesEnabled: 
					if individual[0].graph["sumDistanceFromSources"] == maximumFitness or individual[0].graph["voxelNumber"] == maximumFitness:
						allDone=False
				else:				
					if individual[0].graph["distance"] == -999:
						allDone=False

			# CHECK FOR ANY FITNESS FILES THAT ARE PRESENT
			lsCheck = sub.check_output(["ls",runDirectory+"fitnessFiles/"]) # FC duplicated ids issue: may be due to entering here two times for the same fitness file found in the directory.
			if lsCheck:

				lsCheck = lsCheck.split()[0]
				if "softbotsOutput--id_" in lsCheck:
					thisId = int(lsCheck.split("_")[1].split(".")[0])
					
					if not(thisId in alreadyAnalyzedIds): # FC workaround to avoid the "duplicated ids issue"
						numEvalsFinished += 1
						alreadyAnalyzedIds.append(thisId)

						if sourcesEnabled:
							(minDistanceFromSources, voxelNumber, volumeStart, volumeEnd, chullVolumeStart, chullVolumeEnd) = readSoftbotFitnessFile(runDirectory+"fitnessFiles/"+lsCheck)
							sumDistanceFromSources = sum(minDistanceFromSources) # let's aggregate into a single objective to be minimized
							
							lineToPrint = "%s\tvoxs=%6d\tsumDistSourc=%0.2f\tBIs=%0.3f\tBIe=%0.3f (%5d / %5d)"%(lsCheck,voxelNumber,sumDistanceFromSources,volumeStart/chullVolumeStart,volumeEnd/chullVolumeEnd,numEvalsFinished, numEvaluatedThisGen)
							printLog(lineToPrint)
							#print lsCheck,"\tvoxs=%d"%(voxelNumber),"\tsumDistSourc=%0.2f"%(sumDistanceFromSources),"\tBIs=%0.3f"%(volumeStart/chullVolumeStart),"\tBIe=%0.3f"%(volumeEnd/chullVolumeEnd),"(",numEvalsFinished,"/",numEvaluatedThisGen,")"

						else:
							(thisFitness,thisDistance) = readSoftbotFitnessFile(runDirectory+"fitnessFiles/"+lsCheck)
							print lsCheck,"fit = %0.5f"%(thisDistance),"  (",numEvalsFinished,"/",numEvaluatedThisGen,")"					
						
						sub.call("rm "+runDirectory+"fitnessFiles/"+lsCheck,shell=True) # now that we've read the fitness file, we can remove it
						######################


						# ASSIGN THE VALUE IN THEM TO THE CORRESPONDING INDIVIDUAL
						for individual in subPopulation:
							if individual[0].graph["id"] == thisId:
								if sourcesEnabled:
									individual[0].graph["voxelNumber"] = voxelNumber
									individual[0].graph["sumDistanceFromSources"] = sumDistanceFromSources
									individual[0].graph["volumeStart"] = volumeStart
									individual[0].graph["volumeEnd"] = volumeEnd
									individual[0].graph["chullVolumeStart"] = chullVolumeStart
									individual[0].graph["chullVolumeEnd"] = chullVolumeEnd

									alreadyEvaluated[md5] = (individual[0].graph["voxelNumber"],individual[0].graph["sumDistanceFromSources"],individual[0].graph["energy"],individual[0].graph["shapeGlobalSymmetryIdx"],individual[0].graph["shapeXSymmetryIdxAggregated"],individual[0].graph["shapeYSymmetryIdxAggregated"],individual[0].graph["shapeZSymmetryIdxAggregated"], individual[0].graph["growthGlobalSymmetryIdx"],individual[0].graph["growthXSymmetryIdxAggregated"],individual[0].graph["growthYSymmetryIdxAggregated"],individual[0].graph["growthZSymmetryIdxAggregated"], individual[0].graph["percentExpandingVoxels"])

									if gen%saveVxaEvery == 0 and saveVxaEvery > 0:
										sub.call("cp "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"]+" "+runDirectory+"Gen_%04i/"%(gen)+runName+"--Gen_%04i--id_%05i--voxNum_%07i--distSum_%.08f--BIs_%.08f--BIe_%.08f--sSI_%.08f--gSI_%.08f.vxa"%(gen,individual[0].graph["id"],individual[0].graph["voxelNumber"],individual[0].graph["sumDistanceFromSources"], individual[0].graph["volumeStart"]/individual[0].graph["chullVolumeStart"], individual[0].graph["volumeEnd"]/individual[0].graph["chullVolumeEnd"], individual[0].graph["shapeGlobalSymmetryIdx"], individual[0].graph["growthGlobalSymmetryIdx"]),shell=True)

									# UPDATE THE RUN STATISTICS AND FILE MANAGEMENT
									if individual[0].graph["sumDistanceFromSources"] < bestFitOnlySoFar:
										newBestFound = True;
										bestOfGenFilename = runDirectory+"bestSoFar/fitOnly/"+runName+"--FitOnlyBestOfGen_%04i--id_%05i--voxNum_%07i--distSum_%.08f--BIs_%.08f--BIe_%.08f--sSI_%.08f--sSI_%.08f.vxa"%(gen,individual[0].graph["id"],individual[0].graph["voxelNumber"],individual[0].graph["sumDistanceFromSources"], individual[0].graph["volumeStart"]/individual[0].graph["chullVolumeStart"], individual[0].graph["volumeEnd"]/individual[0].graph["chullVolumeEnd"], individual[0].graph["shapeGlobalSymmetryIdx"], individual[0].graph["growthGlobalSymmetryIdx"])
										bestFitOnlySoFar = individual[0].graph["sumDistanceFromSources"] 
										#sub.call("rm "+runDirectory+"bestSoFar/fitOnly/*.vxa 2>/dev/null",shell=True)
										# override current best of gen
										sub.call("cp "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"]+" "+runDirectory+"bestSoFar/fitOnly/Gen_%04i.vxa"%(gen),shell=True) 
									#else:
										#sub.call("rm "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"],shell=True)								
										#sub.call("cp "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"]+" "+runDirectory+"bestSoFar/fitOnly/"+runName+"--Gen_%04i--distSum_%.08f--voxNum_%d--id_%05i.vxa"%(gen,individual[0].graph["sumDistanceFromSources"],individual[0].graph["voxelNumber"],individual[0].graph["id"]),shell=True)
									break
								else:
									individual[0].graph["distance"] = thisDistance
									# individual[0].graph["fitness"] = thisFitness
									individual[0].graph["fitness"] = thisFitness # * (1/individual[0].graph["energy"])
									alreadyEvaluated[md5] = (individual[0].graph["distance"],individual[0].graph["fitness"],individual[0].graph["energy"])

									# UPDATE THE RUN STATISTICS AND FILE MANAGEMENT
									# if individual[0].graph["distance"] > bestDistOnlySoFar:
									# 	bestDistOnlySoFar = individual[0].graph["distance"]
									# 	sub.call("cp voxelyzeFiles/voxelyzeFile--id_%05i.vxa"%individual[0].graph["id"]+" bestSoFar/distOnly/"+runName+"--Gen_%04i--fit_%.08f--dist_%.08f--height_%06f--freq_%06f--id_%05i.vxa"%(gen,individual[0].graph["fitness"],individual[0].graph["distance"],individual[0].graph["height"],individual[0].graph["frequency"],individual[0].graph["id"]),shell=True)
									# if individual[0].graph["energy"] > bestEnergyOnlySoFar:
									# 	bestEnergyOnlySoFar = individual[0].graph["energy"]
									# 	sub.call("cp voxelyzeFiles/voxelyzeFile--id_%05i.vxa"%individual[0].graph["id"]+" bestSoFar/energyOnly/"+runName+"--Gen_%04i--fit_%.08f--dist_%.08f--height_%06f--freq_%06f--id_%05i.vxa"%(gen,individual[0].graph["fitness"],individual[0].graph["distance"],individual[0].graph["height"],individual[0].graph["frequency"],individual[0].graph["id"]),shell=True)
									if individual[0].graph["fitness"] > bestFitOnlySoFar:
										bestFitOnlySoFar = individual[0].graph["fitness"]
										sub.call("cp "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"]+" "+runDirectory+"bestSoFar/fitOnly/"+runName+"--Gen_%04i--fit_%.08f--dist_%.08f--freq_%06f--id_%05i.vxa"%(gen,individual[0].graph["fitness"],individual[0].graph["distance"],individual[0].graph["frequency"],individual[0].graph["id"]),shell=True)
									if gen%saveVxaEvery == 0 and saveVxaEvery > 0:
										sub.call("mv "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"]+" "+runDirectory+"Gen_%04i/"%(gen)+runName+"--Gen_%04i--fit_%.08f--dist_%.08f--freq_%06f--id_%05i.vxa"%(gen,individual[0].graph["fitness"],individual[0].graph["distance"],individual[0].graph["frequency"],individual[0].graph["id"]),shell=True)
									else:
										sub.call("rm "+runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"],shell=True)
									break

				# WAIT A SEC AND TRY AGAIN		
				else:
					time.sleep(1)
			else:
				time.sleep(1)
		
		if not allDone:
			if restartTheWholeEvaluation:
				printLog("Looks like checkpointing occurred in the middle of evaluateAll. Restarting the whole evaluation process.");
				continue
			else:
				printLog("WARNING: Couldn't get a fitness value in time for some individuals, probably there's something wrong with them (e.g. simulation diverged). The worst fitness is being assigned to those")

		# rename bestOfGen fitness only to include individual's stats in the filename
		if newBestFound:
			sub.call("mv "+runDirectory+"bestSoFar/fitOnly/Gen_%04i.vxa"%(gen)+" "+bestOfGenFilename,shell=True) 	

		# return numEvaluatedThisGen

		endTime = time.time()
		print "\nAll Voxelyze evals finished in",endTime-startTime, "seconds"
		print "numEvaluatedThisGen: "+str(numEvaluatedThisGen)+"/"+str(popSize)
		print "totalEvaluations:",totalEvaluations
		break # break the outer loop

def createRandomNetwork(networkNum):

	G = createMinimalNetwork(networkNum)

	for i in range(numRandomNodes):
		addNode(G,networkNum)

	for i in range(numRandomLinkAdds):
		addLink(G,networkNum)

	for i in range(numRandomWeightChanges):
		mutateWeight(G,networkNum)

	for i in range(numRandomLinkRemovals):
		removeLink(G,networkNum)

	for i in range(numRandomActivationFunction):
		mutFunct(G,networkNum)

	# pruneNetwork(G,networkNum)

	# createPhenotype(G)
	# print "BIAS EDGES:"
	# for thisEdge in G.out_edges(nbunch=["b"]):
	# 	# print thisEdge,G.edge[thisEdge[0]][thisEdge[1]]["weight"]
	# 	G.edge[thisEdge[0]][thisEdge[1]]["weight"] = 0.0

	return G

def createPhenotype(individual,networkNum = -1,sizeX=origSizeX,sizeY=origSizeY,sizeZ=origSizeZ):
	clearNodeStates(individual,networkNum)
	setInputNodeState(individual)
	for networkNum in range(len(outputNodeNames)):
		for outputNode in outputNodeNames[networkNum]:
			individual[networkNum].node[outputNode]["state"] = np.zeros((sizeX,sizeY,sizeZ))
			individual[networkNum].node[outputNode]["state"] = calcNodeState(individual[networkNum],outputNode,sizeX,sizeY,sizeZ)
	if "frequency" in allNetworksOutputs:
		individual[0].graph["frequency"] = 7.5+5.0*max(-0.5,min(0.5,np.mean(individual[1].node["frequency"]["state"])))
	else:
		individual[0].graph["frequency"] = actuationsPerSecond
	# print "frequency output mean:",np.mean(individual[1].node["frequency"]["state"])

def clearNodeStates(individual,networkNum = -1):
	if networkNum >= 0:
		for thisNode in individual[networkNum].nodes():
			individual[networkNum].node[thisNode]["evaluated"]=False
	else:
		for networkNum in range(len(outputNodeNames)):
			for thisNode in individual[networkNum].nodes():
				individual[networkNum].node[thisNode]["evaluated"]=False


def setInputNodeState(individual,sizeX=origSizeX,sizeY=origSizeY,sizeZ=origSizeZ):
	for G in individual:
		inputX = np.zeros((sizeX,sizeY,sizeZ))
		inputY = np.zeros((sizeX,sizeY,sizeZ))
		inputZ = np.zeros((sizeX,sizeY,sizeZ))
		inputD = np.zeros((sizeX,sizeY,sizeZ))
		inputB = np.ones((sizeX,sizeY,sizeZ))

		for x in range(sizeX):
			for y in range(sizeY):
				for z in range(sizeZ):
					# inputX[x,y,z] = ( 2*float( x - sizeX )/sizeX + 1 + 1.0/sizeX) * scalingFactor * (1+1.0/sizeX)
					# inputY[x,y,z] = ( 2*float( y - sizeY )/sizeY + 1 + 1.0/sizeY) * scalingFactor * (1+1.0/sizeY)
					# inputZ[x,y,z] = ( 2*float( z - sizeZ )/sizeZ + 1 + 1.0/sizeZ) * scalingFactor * (1+1.0/sizeZ)
					inputX[x,y,z] = x
					inputY[x,y,z] = y
					inputZ[x,y,z] = z
					
		inputX = normalizeMatrix(inputX)
		inputY = normalizeMatrix(inputY)
		inputZ = normalizeMatrix(inputZ)

		inputD = normalizeMatrix( pow( pow(inputX, 2) + pow(inputY, 2) + pow(inputZ, 2), 0.5 ) ) 

		for thisNode in G.nodes():
			if thisNode == "x":
				G.node[thisNode]["state"]=inputX
				G.node[thisNode]["evaluated"]=True
			if thisNode == "y":
				G.node[thisNode]["state"]=inputY
				G.node[thisNode]["evaluated"]=True
			if thisNode == "z":
				G.node[thisNode]["state"]=inputZ
				G.node[thisNode]["evaluated"]=True
			if thisNode == "d":
				G.node[thisNode]["state"]=inputD
				G.node[thisNode]["evaluated"]=True
			if thisNode == "b":
				G.node[thisNode]["state"]=inputB
				G.node[thisNode]["evaluated"]=True

def calcNodeState(G,thisNode,sizeX=origSizeX,sizeY=origSizeY,sizeZ=origSizeZ):

	if G.node[thisNode]["evaluated"]:
		return G.node[thisNode]["state"]

	G.node[thisNode]["evaluated"]=True
	
	inputEdges = G.in_edges(nbunch=[thisNode])
	newState = np.zeros((sizeX,sizeY,sizeZ))

	# if "id" in G.graph:
	for inputEdge in inputEdges:
		# oldState = np.max(np.abs(newState))
		newState += calcNodeState(G,inputEdge[0],sizeX=sizeX,sizeY=sizeY,sizeZ=sizeZ)*G.edge[inputEdge[0]][inputEdge[1]]["weight"]
		# if np.max(np.abs(newState))==0:	
			# print "input to",thisNode,"from",inputEdge[0],"(",np.mean(G.node[inputEdge[0]]["state"]),")","with weight",G.edge[inputEdge[0]][inputEdge[1]]["weight"],"increases state from",oldState,"to",np.max(np.abs(newState))
		
	G.node[thisNode]["state"] = newState

	if G.node[thisNode]["function"]=="abs":
		newState = np.abs(newState)
	if G.node[thisNode]["function"]=="nAbs":
		newState = -np.abs(newState)
	if G.node[thisNode]["function"]=="sin":
		newState = np.sin(newState)
	if G.node[thisNode]["function"]=="square":
		newState = np.power(newState,2)
	if G.node[thisNode]["function"]=="nSquare":
		newState = -np.power(newState,2)
	if G.node[thisNode]["function"]=="sqrt":
		newState = np.power(np.abs(newState),0.5)
	if G.node[thisNode]["function"]=="nSqrt":
		newState = -np.power(np.abs(newState),0.5)
	# if G.node[thisNode]["function"]=="blur":
	# 	newState = filters.gaussian_filter(newState, sigma=30)
	if G.node[thisNode]["function"]=="edge":
		newState = filters.convolve(newState,edgeMatrix)
	if G.node[thisNode]["function"]=="gradient":
		newState = morphology.morphological_gradient(newState,size=filterSize)
	if G.node[thisNode]["function"]=="erosion":
		newState = morphology.grey_erosion(newState,size=filterSize)
	if G.node[thisNode]["function"]=="dilation":
		newState = morphology.grey_dilation(newState,size=filterSize)
	if G.node[thisNode]["function"]=="opening":
		newState = morphology.grey_opening(newState,size=filterSize)
	if G.node[thisNode]["function"]=="closing":
		newState = morphology.grey_closing(newState,size=filterSize)
	# if G.node[thisNode]["function"]=="skeleton":
	# 	newState = skeletonize(newState[:,:,0])
	# if G.node[thisNode]["function"]=="coral-RD":
	# 	newState = grayScottReactionDiffusion(newState,0.16, 0.08, 0.060, 0.062, 3000)
	# if G.node[thisNode]["function"]=="fingerprint-RD":
	# 	newState = grayScottReactionDiffusion(newState,0.19, 0.05, 0.060, 0.062, 1000)
	# if G.node[thisNode]["function"]=="spiral-RD":
	# 	newState = grayScottReactionDiffusion(newState,0.12, 0.08, 0.020, 0.050, 1200)
	# if G.node[thisNode]["function"]=="unstable-RD":
	# 	newState = grayScottReactionDiffusion(newState,0.16, 0.08, 0.020, 0.055, 1200)
	# if G.node[thisNode]["function"]=="worm-RD":
	# 	newState = grayScottReactionDiffusion(newState,0.16, 0.08, 0.054, 0.063, 3000)
	# if G.node[thisNode]["function"]=="zebrafish-RD":
	# 	newState = grayScottReactionDiffusion(newState,0.16, 0.08, 0.035, 0.060, 1200)


	# print G.node[thisNode]["function"],"[",np.min(newState),np.max(newState),"]"
	
	newState = 2.0/(1.0+np.exp(-newState))-1.0
	# print G.node[thisNode]["function"],"[",np.min(newState),np.max(newState),"]"
	
	# newState = normalizeMatrix(newState)
	# print G.node[thisNode]["function"],"[",np.min(newState),np.max(newState),"]"

	# if not thisNode in outputNodeNames:
	# 	newState *= scalingFactor
	# print G.node[thisNode]["label"],"[",np.min(newState),np.max(newState),"]"

	return newState

def crossoverBoth(individual1,individual2,networkNum):
	global maxID

	G1 = individual1[networkNum]
	G2 = individual2[networkNum]

	hiddenNodes1 = list(set(G1.nodes()) - set(inputNodeNames[networkNum]) - set(outputNodeNames[networkNum]))
	if len(hiddenNodes1) == 0:
		# print "no hidden nodes in G1, returning G2 without crossover"
		return [individual1,individual2]
	crossoverRoot1 = random.choice(hiddenNodes1)

	hiddenNodes2 = list(set(G2.nodes()) - set(inputNodeNames[networkNum]) - set(outputNodeNames[networkNum]))
	if len(hiddenNodes2) == 0:
		# print "no hidden nodes in G1, returning G2 without crossover"
		return [individual1,individual2]
	crossoverRoot2 = random.choice(hiddenNodes2)

	G1new, numNodes1 = crossover(copy.deepcopy(G1),copy.deepcopy(G2),crossoverRoot1,crossoverRoot2,networkNum)
	G2new, numNodes2 = crossover(copy.deepcopy(G2),copy.deepcopy(G1),crossoverRoot2,crossoverRoot1,networkNum)

	newIndividual1 = copy.deepcopy(individual1)
	newIndividual2 = copy.deepcopy(individual2)

	newIndividual1[networkNum] = G1new
	newIndividual2[networkNum] = G2new

	newIndividual1[0].graph["parentID1"]=individual1[0].graph["id"]
	newIndividual1[0].graph["parentID2"]=individual2[0].graph["id"]
	newIndividual2[0].graph["parentID1"]=individual1[0].graph["id"]
	newIndividual2[0].graph["parentID2"]=individual2[0].graph["id"]

	newIndividual1[0].graph["parentMd5Shape1"] = individual1[0].graph["md5Shape"]
	newIndividual2[0].graph["parentMd5Shape1"] = individual1[0].graph["md5Shape"]
	newIndividual1[0].graph["parentMd5Shape2"] = individual2[0].graph["md5Shape"]
	newIndividual2[0].graph["parentMd5Shape2"] = individual2[0].graph["md5Shape"]

	if sourcesEnabled:
		newIndividual1[0].graph["parentDist1"]=individual1[0].graph["voxelNumber"]
		newIndividual1[0].graph["parentDist2"]=individual2[0].graph["voxelNumber"]
		newIndividual2[0].graph["parentDist1"]=individual1[0].graph["voxelNumber"]
		newIndividual2[0].graph["parentDist2"]=individual2[0].graph["voxelNumber"]

		newIndividual1[0].graph["parentFit1"]=individual1[0].graph["sumDistanceFromSources"]
		newIndividual1[0].graph["parentFit2"]=individual2[0].graph["sumDistanceFromSources"]
		newIndividual2[0].graph["parentFit1"]=individual1[0].graph["sumDistanceFromSources"]
		newIndividual2[0].graph["parentFit2"]=individual2[0].graph["sumDistanceFromSources"]
	else:
		newIndividual1[0].graph["parentDist1"]=individual1[0].graph["distance"]
		newIndividual1[0].graph["parentDist2"]=individual2[0].graph["distance"]
		newIndividual2[0].graph["parentDist1"]=individual1[0].graph["distance"]
		newIndividual2[0].graph["parentDist2"]=individual2[0].graph["distance"]

		newIndividual1[0].graph["parentFit1"]=individual1[0].graph["fitness"]
		newIndividual1[0].graph["parentFit2"]=individual2[0].graph["fitness"]
		newIndividual2[0].graph["parentFit1"]=individual1[0].graph["fitness"]
		newIndividual2[0].graph["parentFit2"]=individual2[0].graph["fitness"]

	newIndividual1[0].graph["id"] = maxID
	maxID += 1
	newIndividual2[0].graph["id"] = maxID
	maxID += 1
	newIndividual1[0].graph["age"] = max(individual1[0].graph["age"],individual2[0].graph["age"])
	newIndividual2[0].graph["age"] = max(individual1[0].graph["age"],individual2[0].graph["age"])

	newIndividual1[0].graph["trueAge"] = max(individual1[0].graph["trueAge"],individual2[0].graph["trueAge"])
	newIndividual2[0].graph["trueAge"] = max(individual1[0].graph["trueAge"],individual2[0].graph["trueAge"])

	newIndividual1[0].graph["variationType"] = "crossoverSize"+str(numNodes2+numNodes1)
	newIndividual2[0].graph["variationType"] = "crossoverSize"+str(numNodes2+numNodes1)
	

	# print "crossed",individual1[0].graph["id"],"and",individual2[0].graph["id"],"to make",
	# return [G1new,G2new]
	return [newIndividual1,newIndividual2]

def crossover(G1,G2,crossoverRoot1,crossoverRoot2,networkNum):
	
	for edge in G1.out_edges(nbunch=[crossoverRoot1]):
		G1.remove_edge(edge[0],edge[1])

	subtreeNodes = dfs_tree(G1.reverse(),crossoverRoot1).nodes()

	for thisNode in G1.nodes():
		if not thisNode in subtreeNodes:
			G1.remove_node(thisNode)

	tmpEdges = {}
	for thisNode in inputNodeNames[networkNum]:
		if thisNode in subtreeNodes:
			for thisEdge in G1.out_edges(nbunch=[thisNode]):
				tmpEdges[(thisEdge[0],thisEdge[1])] = G1.edge[thisEdge[0]][thisEdge[1]]["weight"]

	for thisNode in G1.nodes():
		if thisNode in inputNodeNames[networkNum]:
			G1.remove_node(thisNode)

	G2maxHiddenNum = getMaxHiddenNodeIndex(G2)
	newNodeNameDict = {}
	for thisNode in G1.nodes():
		if type(thisNode) is int:
		# if not G1.node[thisNode]["id"] in inputNodeNames[networkNum] and not G1.node[thisNode]["id"] in outputNodeNames[networkNum]:
			newNodeNameDict[thisNode]=thisNode+G2maxHiddenNum
			# print "adding node",thisNode,"as",thisNode+G2maxHiddenNum
		else:
			newNodeNameDict[thisNode]=thisNode
			
	G1 = nx.relabel_nodes(G1,newNodeNameDict)
	
	newG2 = nx.union(G1,G2)
	# for thisNode in inputNodeNames:
	# 	if thisNode in G1.nodes():
	# 		for thisEdge in G1.out_edges(nbunch=[thisNode]):
	# 			newG2.add_edge(thisEdge[0],thisEdge[1],weight=G1.edge[thisEdge[0]][thisEdge[1]]["weight"])
	# print "tmpEdges:",tmpEdges

	for thisEdge in tmpEdges:
		if thisEdge[1] in outputNodeNames: # <------- CHECK THIS IF WE'LL EVER USE CROSSOVER AGAIN
			newG2.add_edge(thisEdge[0],thisEdge[1],weight=tmpEdges[(thisEdge[0],thisEdge[1])])
		else:
			newG2.add_edge(thisEdge[0],thisEdge[1]+G2maxHiddenNum,weight=tmpEdges[(thisEdge[0],thisEdge[1])])

	for thisEdge in G2.out_edges(nbunch=[crossoverRoot2]):
		newG2.add_edge(crossoverRoot1+G2maxHiddenNum,thisEdge[1],weight=G2.edge[thisEdge[0]][thisEdge[1]]["weight"])
		
	return newG2, len(G1.nodes()) 

def createMinimalNetwork(networkNum):
	G = nx.DiGraph()

	for thisInputNodeName in inputNodeNames[networkNum]:
		G.add_node(thisInputNodeName,nodeType="inputNode",function="none")
	for thisOutputNodeName in outputNodeNames[networkNum]:
		G.add_node(thisOutputNodeName,nodeType="outputNode",function="sigmoid")

	for inputNode in nx.nodes(G):
		if G.node[inputNode]["nodeType"]=="inputNode":
			for outputNode in nx.nodes(G):
				if G.node[outputNode]["nodeType"]=="outputNode":
					G.add_edge(inputNode,outputNode,weight=0.0)
	return G


def addNode(G,networkNum):
	#-----------------------------------------------------------------------------------
	# CHOOSE TWO RANDOM NODES (between which a link could exist)
	if len(G.edges())==0:
		print "no edges in graph!"
		return "NoEdges"
	thisEdge = random.choice(G.edges())
	node1 = thisEdge[0]
	node2 = thisEdge[1]

	#-----------------------------------------------------------------------------------
	# CREATE A NEW NODE HANGING FROM THE PREVIOUS OUTPUT NODE
	newNodeIndex = getMaxHiddenNodeIndex(G)
	G.add_node(newNodeIndex,nodeType="hiddenNode",function=random.choice(activationFunctionNames)) # random activation function here to solve the problem with admissible mutations in the first 
	#G.add_node(newNodeIndex,nodeType="hiddenNode",function="sigmoid") 			       # <-- was	
	G.add_edge(newNodeIndex,node2,weight=1.0)
	#-----------------------------------------------------------------------------------
	# IF THIS EDGE ALREADY EXISTED HERE, REMOVE IT
	# BUT USE IT'S WEIGHT TO MINIMIZE DISRUPTION WHEN CONNECTING TO PREVIOUS INPUT NODE
	if (node1,node2) in nx.edges(G):
		# print "node from",node1,"to",node2,"exists"
		weight = G.edge[node1][node2]["weight"]
		G.remove_edge(node1,node2)
		G.add_edge(node1,newNodeIndex,weight=weight)
	#-----------------------------------------------------------------------------------
	# IF NOT, USE A WEIGHT OF ZERO (to minimize disruption of new edge)

	else:
		G.add_edge(node1,newNodeIndex,weight=1.0) # was weight=0.0. Setting it to 1.0 should help in finding admissible mutations in the first generations 
	#-----------------------------------------------------------------------------------
	return ""

def addLink(G,networkNum):

	done = False
	attempt = 0
	while not done:
		done = True
		#-----------------------------------------------------------------------------------
		# CHOOSE TWO RANDOM NODES (between which a link could exist, but doesn't)
		node1 = random.choice(G.nodes())
		node2 = random.choice(G.nodes())
		while (not newEdgeIsValid(G,node1,node2)) and attempt<999:
			node1 = random.choice(G.nodes())
			node2 = random.choice(G.nodes())
			attempt += 1
		if attempt > 999:
			print "no valid edges to add found in 1000 attempts."
			done = True
			# return False
		#-----------------------------------------------------------------------------------
		# CREATE A LINK BETWEEN THEM
		if random.random() > 0.5:
			G.add_edge(node1,node2,weight=0.1)
		else:
			G.add_edge(node1,node2,weight=-0.1)
	
		#-----------------------------------------------------------------------------------
		# IF THE LINK CREATES A CYCLIC GRAPH, ERASE IT AND TRY AGAIN
		if hasCycles(G):
			G.remove_edge(node1,node2)
			done = False
			attempt += 1
		if attempt > 999:
			print "no valid edges to add found in 1000 attempts."
			done = True
		#-----------------------------------------------------------------------------------
	return ""

def removeLink(G,networkNum):
	if len(G.edges())==0:
		return "NoEdges"
	thisLink = random.choice(G.edges())
	G.remove_edge(thisLink[0],thisLink[1])
	return ""

def resetNetwork(G,networkNum):
	G = createRandomNetwork(networkNum)
	return ""

def removeNode(G,networkNum):

	hiddenNodes = list(set(G.nodes()) - set(inputNodeNames[networkNum]) - set(outputNodeNames[networkNum]))
	if len(hiddenNodes)==0:
		return "NoHidden"
	thisNode = random.choice(hiddenNodes)

	# if there are edge paths going through this node, keep them connected to minimize disruption
	incomingEdges = G.in_edges(nbunch=[thisNode])
	outgoingEdges = G.out_edges(nbunch=[thisNode])

	for incomingEdge in incomingEdges:
		for outgoingEdge in outgoingEdges:
			G.add_edge(incomingEdge[0],outgoingEdge[1],weight=G.edge[incomingEdge[0]][thisNode]["weight"]*G.edge[thisNode][outgoingEdge[1]]["weight"])

	G.remove_node(thisNode)
	return ""

def pruneNetwork(individual,networkNum):

	done = False
	while not done:
		done = True
		for thisNode in individual[networkNum].nodes():
			if len(individual[networkNum].in_edges(nbunch=[thisNode]))==0 and not thisNode in inputNodeNames[networkNum] and not thisNode in outputNodeNames[networkNum]:
				# print "pruning node",thisNode
				individual[networkNum].remove_node(thisNode)
				done = False
		for thisNode in individual[networkNum].nodes():
			if len(individual[networkNum].out_edges(nbunch=[thisNode]))==0 and not thisNode in inputNodeNames[networkNum] and not thisNode in outputNodeNames[networkNum]:
				# print "pruning node",thisNode
				individual[networkNum].remove_node(thisNode)
				done = False


def mutateWeight(G,networkNum):
	if len(G.edges())==0:
		print "Graph has no edges to mutate!"
		return "NoEdges"
	thisEdge = random.choice(G.edges())
	node1 = thisEdge[0]
	node2 = thisEdge[1]
	oldWeight = G[node1][node2]["weight"]
	newWeight = oldWeight
	while oldWeight == newWeight:
		newWeight = random.gauss(oldWeight,mutationStd)
		newWeight = max(-1.0,min(newWeight,1.0))
	G[node1][node2]["weight"]=newWeight
	return "%.04f"%(newWeight-oldWeight)

def mutFunct(G,networkNum):
	thisNode = random.choice(G.nodes())
	while thisNode in inputNodeNames[networkNum]:
		thisNode = random.choice(G.nodes())
	oldFunction = G.node[thisNode]["function"]
	while G.node[thisNode]["function"] == oldFunction:
		G.node[thisNode]["function"] = random.choice(activationFunctionNames)
	return "_"+oldFunction+"-to-"+G.node[thisNode]["function"]

def hasCycles(G):
	return sum(1 for e in nx.simple_cycles(G)) != 0

def getMaxHiddenNodeIndex(G):
	maxIndex = 0
	for inputNode in nx.nodes(G):
		if G.node[inputNode]["nodeType"]=="hiddenNode" and int(inputNode)>=maxIndex:
			maxIndex = inputNode + 1
	return maxIndex

def newEdgeIsValid(G,node1,node2):
	if node1==node2:
		return False
	if G.node[node1]['nodeType'] == "outputNode":
		return False
	if G.node[node2]['nodeType'] == "inputNode":
		return False
	if (node2,node1) in nx.edges(G):
		return False
	if (node1,node2) in nx.edges(G):
		return False
	return True

def normalizeMatrix(matrix):
	# print "max:",np.max(matrix),"min:",np.min(matrix)
	matrix -= np.min(matrix)
	# print "max:",np.max(matrix),"min:",np.min(matrix)
	matrix /= np.max(matrix)
	# print "max:",np.max(matrix),"min:",np.min(matrix)
	matrix = np.nan_to_num(matrix)
	# print "max:",np.max(matrix),"min:",np.min(matrix)
	matrix *= 2
	# print "max:",np.max(matrix),"min:",np.min(matrix)
	matrix -= 1
	# print "max:",np.max(matrix),"min:",np.min(matrix)
	# print matrix
	# exit(0)
	return matrix

# def grayScottReactionDiffusion(matrix,Du, Dv, F, k, t):
# 	# From: Nicolas P. Rougier (http://www.labri.fr/perso/nrougier/teaching/numpy/numpy.html)
# 	# Parameters from http://www.aliensaint.com/uo/java/rd/
# 	# -----------------------------------------------------
# 	# Du, Dv, F, k = 0.16, 0.08, 0.035, 0.065 # Bacteria 1
# 	# Du, Dv, F, k = 0.14, 0.06, 0.035, 0.065 # Bacteria 2
# 	# Du, Dv, F, k = 0.16, 0.08, 0.060, 0.062 # Coral
# 	# Du, Dv, F, k = 0.19, 0.05, 0.060, 0.062 # Fingerprint
# 	# Du, Dv, F, k = 0.10, 0.10, 0.018, 0.050 # Spirals
# 	# Du, Dv, F, k = 0.12, 0.08, 0.020, 0.050 # Spirals Dense
# 	# Du, Dv, F, k = 0.10, 0.16, 0.020, 0.050 # Spirals Fast
# 	# Du, Dv, F, k = 0.16, 0.08, 0.020, 0.055 # Unstable
# 	# Du, Dv, F, k = 0.16, 0.08, 0.050, 0.065 # Worms 1
# 	# Du, Dv, F, k = 0.16, 0.08, 0.054, 0.063 # Worms 2
# 	# Du, Dv, F, k = 0.16, 0.08, 0.035, 0.060 # Zebrafish

# 	sizeX=matrix.shape[0]
# 	sizeY=matrix.shape[1]
# 	sizeZ=matrix.shape[2]

# 	Z = np.zeros((sizeX+2,sizeY+2), [('U', np.double), ('V', np.double)])
# 	U,V = Z['U'], Z['V']
# 	u,v = U[1:-1,1:-1], V[1:-1,1:-1]

# 	r = 20
# 	u[...] = 1.0
# 	# U[n/2-r:n/2+r,n/2-r:n/2+r] = 0.50
# 	# V[n/2-r:n/2+r,n/2-r:n/2+r] = 0.25
# 	# get shape of mateix
# 	# U[1:-1,1:-1] = (matrix[:,:,0]>0)*0.5
# 	# V[1:-1,1:-1] = (matrix[:,:,0]>0)*0.25
# 	U[1:-1,1:-1] = (matrix[:,:,0]/scalingFactor/2.0+0.5)*0.5*1.5
# 	V[1:-1,1:-1] = (matrix[:,:,0]/scalingFactor/2.0+0.5)*0.25*1.5
# 	# calculate edges
# 	U[0,:]=U[1,:]
# 	U[-1,:]=U[-2,:]
# 	V[0,:]=V[1,:]
# 	V[-1,:]=V[-2,:]
# 	U[:,0]=U[:,1]
# 	U[:,-1]=U[:,-2]
# 	V[:,0]=V[:,1]
# 	V[:,-1]=V[:,-2]
# 	u += 0.05*np.random.random((sizeX,sizeY))
# 	v += 0.05*np.random.random((sizeX,sizeY))

# 	for i in xrange(t):
# 		Lu = (                 U[0:-2,1:-1] +
# 			  U[1:-1,0:-2] - 4*U[1:-1,1:-1] + U[1:-1,2:] +
# 							   U[2:  ,1:-1] )
# 		Lv = (                 V[0:-2,1:-1] +
# 			  V[1:-1,0:-2] - 4*V[1:-1,1:-1] + V[1:-1,2:] +
# 							   V[2:  ,1:-1] )

# 		uvv = u*v*v
# 		u += (Du*Lu - uvv +  F   *(1-u))
# 		v += (Dv*Lv + uvv - (F+k)*v    )

# 	return np.array(V[1:-1,1:-1]).reshape((sizeX,sizeY,sizeZ))

def shiftFold(matrix,c):
	# todo: implement for fractals
	0


def writeVoxelyzeFileOrig(individual):
	voxelyzeFile = open(runDirectory+"voxelyzeFiles/"+runName+"--id_%05i.vxa"%individual[0].graph["id"],"w")
	voxelyzeFile.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n\
<VXA Version=\"1.0\">\n\
<Simulator>\n\
<Integration>\n\
<Integrator>0</Integrator>\n\
<DtFrac>"+str(dtFrac)+"</DtFrac>\n\
</Integration>\n\
<Damping>\n\
<BondDampingZ>1</BondDampingZ>\n\
<ColDampingZ>0.8</ColDampingZ>\n\
<SlowDampingZ>0.01</SlowDampingZ>\n\
</Damping>\n\
<Collisions>\n\
<SelfColEnabled>"+str(int(SelfCollisionsEnabled))+"</SelfColEnabled>\n\
<ColSystem>3</ColSystem>\n\
<CollisionHorizon>2</CollisionHorizon>\n\
</Collisions>\n\
<Features>\n\
<FluidDampEnabled>0</FluidDampEnabled>\n\
<PoissonKickBackEnabled>0</PoissonKickBackEnabled>\n\
<EnforceLatticeEnabled>0</EnforceLatticeEnabled>\n\
</Features>\n\
<SurfMesh>\n\
<CMesh>\n\
<DrawSmooth>1</DrawSmooth>\n\
<Vertices/>\n\
<Facets/>\n\
<Lines/>\n\
</CMesh>\n\
</SurfMesh>\n\
<StopCondition>\n")
	if combinedStopCondition:
		voxelyzeFile.write("<StopConditionType>7</StopConditionType>\n")
	else:
		voxelyzeFile.write("<StopConditionType>2</StopConditionType>\n")
	voxelyzeFile.write("<StopConditionValue>"+str(simulationTime)+"</StopConditionValue>\n") # was: fitnessEvaluationCycles/individual[0].graph["frequency"]+fitnessEvalInitTime
	voxelyzeFile.write("<InitCmTime>"+str(fitnessEvalInitTime)+"</InitCmTime>\n\
</StopCondition>\n\
<EquilibriumMode>\n\
<EquilibriumModeEnabled>"+str(equilibriumMode)+"</EquilibriumModeEnabled>\n\
</EquilibriumMode>\n\
<GA>\n\
<WriteFitnessFile>1</WriteFitnessFile>\n\
<FitnessFileName>"+runDirectory+"fitnessFiles/softbotsOutput--id_%05i.xml"%individual[0].graph["id"]+"</FitnessFileName>\n\
<QhullTmpFile>"+runDirectory+"tempFiles/qhullInput--id_%05i.txt"%individual[0].graph["id"]+"</QhullTmpFile>\n\
</GA>\n\
<MinTempFact>"+str(minTempFact)+"</MinTempFact>\n\
<MaxTempFactChange>"+str(maxTempFactChange)+"</MaxTempFactChange>\n\
</Simulator>\n\
<Environment>\n")
	if floorLimited:
		voxelyzeFile.write("<FloorRadius>"+str(floorRadius)+"</FloorRadius>\n")

	voxelyzeFile.write("<GrowthAmplitude>"+str(growthAmplitude)+"</GrowthAmplitude>\n")

	if inCage:
		voxelyzeFile.write("<Boundary_Conditions>\n\
<NumBCs>4</NumBCs>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>0</Y>\n\
<Z>0</Z>\n\
<dX>0.01</dX>\n\
<dY>1</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>0</Y>\n\
<Z>0</Z>\n\
<dX>1</dX>\n\
<dY>0.01</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0.99</X>\n\
<Y>0</Y>\n\
<Z>0</Z>\n\
<dX>0.01</dX>\n\
<dY>1</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>0.99</Y>\n\
<Z>0</Z>\n\
<dX>1</dX>\n\
<dY>0.01</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
</Boundary_Conditions>\n")
	
	elif swarmClimb:
		voxelyzeFile.write("<Boundary_Conditions>\n\
<NumBCs>1</NumBCs>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>0.5</Y>\n\
<Z>0</Z>\n\
<dX>1</dX>\n\
<dY>0.5</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
</Boundary_Conditions>\n")

	elif numCircles*interCircleDist>0:
		voxelyzeFile.write("<Boundary_Conditions>\n\
<NumBCs>4</NumBCs>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>0</Y>\n\
<Z>0</Z>\n\
<dX>"+str(float(numCircles*interCircleDist-1)/(origSizeX+numCircles*interCircleDist*2))+"</dX>\n\
<dY>1</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>"+str(1-float(numCircles*interCircleDist-1)/(origSizeX+numCircles*interCircleDist*2))+"</X>\n\
<Y>0</Y>\n\
<Z>0</Z>\n\
<dX>"+str(float(numCircles*interCircleDist-1)/(origSizeX+numCircles*interCircleDist*2))+"</dX>\n\
<dY>1</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>0</Y>\n\
<Z>0</Z>\n\
<dX>1</dX>\n\
<dY>"+str(float(numCircles*interCircleDist-1)/(origSizeY+numCircles*interCircleDist*2))+"</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>"+str(1-float(numCircles*interCircleDist-1)/(origSizeY+numCircles*interCircleDist*2))+"</Y>\n\
<Z>0</Z>\n\
<dX>1</dX>\n\
<dY>"+str(float(numCircles*interCircleDist-1)/(origSizeY+numCircles*interCircleDist*2))+"</dY>\n\
<dZ>1</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
</Boundary_Conditions>\n")
	elif stickyFloor:
		voxelyzeFile.write("<Boundary_Conditions>\n\
<NumBCs>1</NumBCs>\n\
<FRegion>\n\
<PrimType>0</PrimType>\n\
<X>0</X>\n\
<Y>0</Y>\n\
<Z>0</Z>\n\
<dX>1</dX>\n\
<dY>1</dY>\n\
<dZ>0.01</dZ>\n\
<Radius>0</Radius>\n\
<R>0.4</R>\n\
<G>0.6</G>\n\
<B>0.4</B>\n\
<alpha>1</alpha>\n\
<DofFixed>63</DofFixed>\n\
<ForceX>0</ForceX>\n\
<ForceY>0</ForceY>\n\
<ForceZ>0</ForceZ>\n\
<TorqueX>0</TorqueX>\n\
<TorqueY>0</TorqueY>\n\
<TorqueZ>0</TorqueZ>\n\
<DisplaceX>0</DisplaceX>\n\
<DisplaceY>0</DisplaceY>\n\
<DisplaceZ>0</DisplaceZ>\n\
<AngDisplaceX>0</AngDisplaceX>\n\
<AngDisplaceY>0</AngDisplaceY>\n\
<AngDisplaceZ>0</AngDisplaceZ>\n\
</FRegion>\n\
</Boundary_Conditions>\n")
	else:
		voxelyzeFile.write("<Fixed_Regions>\n\
<NumFixed>0</NumFixed>\n\
</Fixed_Regions>\n\
<Forced_Regions>\n\
<NumForced>0</NumForced>\n\
</Forced_Regions>\n")


	voxelyzeFile.write("<Gravity>\n\
<GravEnabled>1</GravEnabled>\n\
<GravAcc>-9.81</GravAcc>\n\
<FloorEnabled>"+str(floorEnabled)+"</FloorEnabled>\n\
<FloorSlope>"+str(floorSlope)+"</FloorSlope>\n\
</Gravity>\n\
<Thermal>\n\
<TempEnabled>"+str(tempEnabled)+"</TempEnabled>\n\
<TempAmp>39</TempAmp>\n\
<TempBase>25</TempBase>\n\
<VaryTempEnabled>1</VaryTempEnabled>\n\
<TempPeriod>"+str(1.0/(individual[0].graph["frequency"]))+"</TempPeriod>\n\
</Thermal>\n")

	if sourcesEnabled and len(sourcesTags) > 0:
		voxelyzeFile.write("<Sources>\n")
		# R - L 	right, left
		# U - D		up, down
		# F - B		front, back
		sourceNo = 0

		for tag in sourcesTags:

			dX = latticeDimension*origSizeX
			dY = latticeDimension*origSizeY
			dZ = latticeDimension*origSizeZ

			# Starts from the centroid of the workspace
			posX = dX/2;
			posY = dY/2;
			posZ = dZ/2;

			posX += tag.count("F")*sourcesDistMul*((1+growthAmplitude)*((2+np.sqrt(2))*dX) + 0.5*np.sqrt(2)*dX)
			posX -= tag.count("B")*sourcesDistMul*((1+growthAmplitude)*((2+np.sqrt(2))*dX) + 0.5*np.sqrt(2)*dX)
			posY += tag.count("R")*sourcesDistMul*((1+growthAmplitude)*((2+np.sqrt(2))*dY) + 0.5*np.sqrt(2)*dY)
			posY -= tag.count("L")*sourcesDistMul*((1+growthAmplitude)*((2+np.sqrt(2))*dY) + 0.5*np.sqrt(2)*dY)			
			posZ += tag.count("U")*sourcesDistMul*((1+growthAmplitude)*((2+np.sqrt(2))*dZ) + 0.5*np.sqrt(2)*dZ)			
			posZ -= tag.count("D")*sourcesDistMul*((1+growthAmplitude)*((2+np.sqrt(2))*dZ) + 0.5*np.sqrt(2)*dZ)

			voxelyzeFile.write("<Source> \n\
	<SourceName>Source"+str(sourceNo)+"</SourceName>\n\
	<SourceType>0</SourceType>\n\
	<SourceEnabled>1</SourceEnabled>\n\
	<SourcePositionX>"+ str(posX) +"</SourcePositionX>\n\
	<SourcePositionY>"+ str(posY) +"</SourcePositionY>\n\
	<SourcePositionZ>"+ str(posZ)   +"</SourcePositionZ>\n\
	<SourceMotionAmpX>"  + str(sourcesMotionTags[sourceNo][0]) +"</SourceMotionAmpX>\n\
	<SourceMotionFreqX>" + str(sourcesMotionTags[sourceNo][1]) +"</SourceMotionFreqX>\n\
	<SourceMotionAmpY>"  + str(sourcesMotionTags[sourceNo][2]) +"</SourceMotionAmpY>\n\
	<SourceMotionFreqY>" + str(sourcesMotionTags[sourceNo][3]) +"</SourceMotionFreqY>\n\
	<SourceMotionAmpZ>"  + str(sourcesMotionTags[sourceNo][4]) +"</SourceMotionAmpZ>\n\
	<SourceMotionFreqZ>" + str(sourcesMotionTags[sourceNo][5]) +"</SourceMotionFreqZ>\n\
	</Source>\n")
			sourceNo += 1
#		voxelyzeFile.write("SourceMotionAmpX<Source> \n\
#<SourceName>Source2</SourceName>\n\
#<SourceType>0</SourceType>\n\
#<SourceEnabled>1</SourceEnabled>\n\
#<SourcePositionX>"+ str(latticeDimension*origSizeX/2) +"</SourcePositionX>\n\
#<SourcePositionY>"+ str(latticeDimension*origSizeY + 4*latticeDimension*origSizeY)+"</SourcePositionY>\n\
#<SourcePositionZ>"+ str(latticeDimension*origSizeZ/2) +"</SourcePositionZ>\n\
#</Source>\n")
		voxelyzeFile.write("</Sources>\n")

	voxelyzeFile.write("</Environment>\n\
<VXC Version=\"0.93\">\n\
<Lattice>\n\
<Lattice_Dim>"+str(latticeDimension)+"</Lattice_Dim>\n\
<X_Dim_Adj>1</X_Dim_Adj>\n\
<Y_Dim_Adj>1</Y_Dim_Adj>\n\
<Z_Dim_Adj>1</Z_Dim_Adj>\n\
<X_Line_Offset>0</X_Line_Offset>\n\
<Y_Line_Offset>0</Y_Line_Offset>\n\
<X_Layer_Offset>0</X_Layer_Offset>\n\
<Y_Layer_Offset>0</Y_Layer_Offset>\n\
</Lattice>\n\
<Voxel>\n\
<Vox_Name>BOX</Vox_Name>\n\
<X_Squeeze>1</X_Squeeze>\n\
<Y_Squeeze>1</Y_Squeeze>\n\
<Z_Squeeze>1</Z_Squeeze>\n\
</Voxel>\n\
<Palette>\n\
<Material ID=\"1\">\n\
<MatType>0</MatType>\n\
<Name>Passive_Soft</Name>\n\
<Display>\n\
<Red>0</Red>\n\
<Green>1</Green>\n\
<Blue>1</Blue>\n\
<Alpha>1</Alpha>\n\
</Display>\n\
<Mechanical>\n\
<MatModel>0</MatModel>\n\
<Elastic_Mod>"+str(softestMaterial)+"e+006</Elastic_Mod>\n\
<Plastic_Mod>0</Plastic_Mod>\n\
<Yield_Stress>0</Yield_Stress>\n\
<FailModel>0</FailModel>\n\
<Fail_Stress>0</Fail_Stress>\n\
<Fail_Strain>0</Fail_Strain>\n\
<Density>1e+006</Density>\n\
<Poissons_Ratio>0.35</Poissons_Ratio>\n\
<CTE>0</CTE>\n\
<uStatic>1</uStatic>\n\
<uDynamic>0.5</uDynamic>\n\
</Mechanical>\n\
</Material>\n\
<Material ID=\"2\">\n\
<MatType>0</MatType>\n\
<Name>Passive_Hard</Name>\n\
<Display>\n\
<Red>0</Red>\n\
<Green>0</Green>\n\
<Blue>1</Blue>\n\
<Alpha>1</Alpha>\n\
</Display>\n\
<Mechanical>\n\
<MatModel>0</MatModel>\n\
<Elastic_Mod>"+str(softestMaterial)+"e+008</Elastic_Mod>\n\
<Plastic_Mod>0</Plastic_Mod>\n\
<Yield_Stress>0</Yield_Stress>\n\
<FailModel>0</FailModel>\n\
<Fail_Stress>0</Fail_Stress>\n\
<Fail_Strain>0</Fail_Strain>\n\
<Density>1e+006</Density>\n\
<Poissons_Ratio>0.35</Poissons_Ratio>\n\
<CTE>0</CTE>\n\
<uStatic>1</uStatic>\n\
<uDynamic>0.5</uDynamic>\n\
</Mechanical>\n\
</Material>\n\
<Material ID=\"3\">\n\
<MatType>0</MatType>\n\
<Name>Active_+</Name>\n\
<Display>\n\
<Red>1</Red>\n\
<Green>0</Green>\n\
<Blue>0</Blue>\n\
<Alpha>1</Alpha>\n\
</Display>\n\
<Mechanical>\n\
<MatModel>0</MatModel>\n\
<Elastic_Mod>"+str(materialStiffness)+"</Elastic_Mod>\n\
<Plastic_Mod>0</Plastic_Mod>\n\
<Yield_Stress>0</Yield_Stress>\n\
<FailModel>0</FailModel>\n\
<Fail_Stress>0</Fail_Stress>\n\
<Fail_Strain>0</Fail_Strain>\n\
<Density>1e+006</Density>\n\
<Poissons_Ratio>0.35</Poissons_Ratio>\n\
<CTE>0.01</CTE>\n\
<uStatic>1</uStatic>\n\
<uDynamic>0.5</uDynamic>\n\
</Mechanical>\n\
</Material>\n\
<Material ID=\"4\">\n\
<MatType>0</MatType>\n\
<Name>Active_-</Name>\n\
<Display>\n\
<Red>0</Red>\n\
<Green>1</Green>\n\
<Blue>0</Blue>\n\
<Alpha>1</Alpha>\n\
</Display>\n\
<Mechanical>\n\
<MatModel>0</MatModel>\n\
<Elastic_Mod>"+str(softestMaterial)+"e+006</Elastic_Mod>\n\
<Plastic_Mod>0</Plastic_Mod>\n\
<Yield_Stress>0</Yield_Stress>\n\
<FailModel>0</FailModel>\n\
<Fail_Stress>0</Fail_Stress>\n\
<Fail_Strain>0</Fail_Strain>\n\
<Density>1e+006</Density>\n\
<Poissons_Ratio>0.35</Poissons_Ratio>\n\
<CTE>-0.01</CTE>\n\
<uStatic>1</uStatic>\n\
<uDynamic>0.5</uDynamic>\n\
</Mechanical>\n\
</Material>\n\
<Material ID=\"5\">\n\
<MatType>0</MatType>\n\
<Name>Aperture</Name>\n\
<Display>\n\
<Red>1</Red>\n\
<Green>0.784</Green>\n\
<Blue>0</Blue>\n\
<Alpha>1</Alpha>\n\
</Display>\n\
<Mechanical>\n\
<MatModel>0</MatModel>\n\
<Elastic_Mod>5e+007</Elastic_Mod>\n\
<Plastic_Mod>0</Plastic_Mod>\n\
<Yield_Stress>0</Yield_Stress>\n\
<FailModel>0</FailModel>\n\
<Fail_Stress>0</Fail_Stress>\n\
<Fail_Strain>0</Fail_Strain>\n\
<Density>1e+006</Density>\n\
<Poissons_Ratio>0.35</Poissons_Ratio>\n\
<CTE>0</CTE>\n\
<uStatic>1</uStatic>\n\
<uDynamic>0.5</uDynamic>\n\
</Mechanical>\n\
</Material>\n\
</Palette>\n\
<Structure Compression=\"ASCII_READABLE\">\n\
<X_Voxels>"+str(origSizeX+4*inCage+numCircles*interCircleDist*2)+"</X_Voxels>\n\
<Y_Voxels>"+str(origSizeY+4*inCage+1*swarmClimb+numCircles*interCircleDist*2)+"</Y_Voxels>\n\
<Z_Voxels>"+str(origSizeZ+1*inCage)+"</Z_Voxels>\n\
<Data>\n")

	# print "muscleType:",np.min(individual[1*("materialMuscleType" in outputNodeNames[1])].node["materialMuscleType"]["state"]),np.max(individual[1*("materialMuscleType" in outputNodeNames[1])].node["materialMuscleType"]["state"])

	shapeMatrix = makeShapeMaxtrix(individual)
	zOffset = 0
	for z in range(origSizeZ):
		if np.sum(shapeMatrix[:,:,z]) == 0:
			zOffset += 1
			#print shapeMatrix[:,:,z]
		else:
			break

	stringForMd5 = ""
	materialCounts = np.zeros((6))
	makeOneShapeOnly(individual)
	for z in range(zOffset,origSizeZ):
		voxelyzeFile.write("<Layer><![CDATA[")
		for y in range(origSizeY):		
			for x in range(origSizeX):

				if enforcingSolidBase and (z == zOffset): # if this is the first non-null z layer and we want a solid base, let's fill it no matter what's in oneShapeOnly
					voxelyzeFile.write("3")
					stringForMd5 += "3"
					materialCounts[3] += 1
				elif individual[netId("materialPresent")].node["materialPresent"]["oneShapeOnly"][x,y,z] <= 0:
					voxelyzeFile.write("0")
					stringForMd5 += "0"
					materialCounts[0] += 1
					# print "TYPE EMPTY"
				elif ("materialHardOrSoft" in allNetworksOutputs) and (individual[netId("materialHardOrSoft")].node["materialHardOrSoft"]["state"][x,y,z] > 0):
						voxelyzeFile.write("2")
						stringForMd5 += "2"
						materialCounts[2] += 1
						# print "TYPE HARD"
				elif ("materialMuscleOrTissue" not in allNetworksOutputs) or (individual[netId("materialMuscleOrTissue")].node["materialMuscleOrTissue"]["state"][x,y,z] > 0):
					# if individual[1*("materialMuscleType" in outputNodeNames[1])].node["materialMuscleType"]["state"][x,y,z] <= 0:
					# 	voxelyzeFile.write("3")
					# 	stringForMd5 += "3"
					# 	materialCounts[3] += 1
					# 	# print "TYPE MUSCLE 1"
					# else:
					# 	voxelyzeFile.write("4")
					# 	stringForMd5 += "4"
					# 	materialCounts[4] += 1
					# 	# print "TYPE MUSCLE 2"
					voxelyzeFile.write("3")
					stringForMd5 += "3"
					materialCounts[3] += 1
				else:			
					voxelyzeFile.write("1")
					stringForMd5 += "1"
					materialCounts[1] += 1
					# print "TYPE SOFT"

		voxelyzeFile.write("]]></Layer>\n")

	# if shifting down, fill in top with zeros
	for z in range(zOffset):
		# print "SHIFITNG DOWN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		voxelyzeFile.write("<Layer><![CDATA[")
		for y in range(origSizeY):		
			for x in range(origSizeX):
				voxelyzeFile.write("0")
				stringForMd5 += "0"
				materialCounts[0] += 1
		voxelyzeFile.write("]]></Layer>\n")


	voxelyzeFile.write("</Data>\n")

	stringForMd5Shape = copy.deepcopy(stringForMd5)
	stringForMd5Control = ""

	if "phaseOffset" in allNetworksOutputs:
		voxelyzeFile.write("<PhaseOffset>\n")
		for z in range(origSizeZ+1*inCage):
			voxelyzeFile.write("<Layer><![CDATA[")
			# for y in range(origSizeY):
			# 	for x in range(origSizeX):
			for y in range(-2*inCage-numCircles*interCircleDist,origSizeY+2*inCage+numCircles*interCircleDist):

				if swarmClimb and y == int(origSizeY/2):
					for x in range(-2*inCage-numCircles*interCircleDist,origSizeX+2*inCage+numCircles*interCircleDist):
						voxelyzeFile.write("0, ")

				for x in range(-2*inCage-numCircles*interCircleDist,origSizeX+2*inCage+numCircles*interCircleDist):
					if y < 0 or y >= origSizeY or x < 0 or x >= origSizeX or z >= origSizeZ:
						voxelyzeFile.write("0, ")
					
					elif swarmClimb and y > int(origSizeY/2):
						voxelyzeFile.write("0, ")

					else:
						voxelyzeFile.write(str(individual[netId("phaseOffset")].node["phaseOffset"]["state"][x,y,z])+", ")
						stringForMd5 += str(individual[netId("phaseOffset")].node["phaseOffset"]["state"][x,y,z])
						stringForMd5Control += str(individual[netId("phaseOffset")].node["phaseOffset"]["state"][x,y,z])
			voxelyzeFile.write("]]></Layer>\n")

		voxelyzeFile.write("</PhaseOffset>\n")


	if "growthRate" in allNetworksOutputs:

		voxelyzeFile.write("<GrowthRate>\n")

		for z in range(origSizeZ+1*inCage):
			voxelyzeFile.write("<Layer><![CDATA[")
			# for y in range(origSizeY):
			# 	for x in range(origSizeX):
			for y in range(-2*inCage-numCircles*interCircleDist,origSizeY+2*inCage+numCircles*interCircleDist):

				if swarmClimb and y == int(origSizeY/2):
					for x in range(-2*inCage-numCircles*interCircleDist,origSizeX+2*inCage+numCircles*interCircleDist):
						voxelyzeFile.write("0, ")

				for x in range(-2*inCage-numCircles*interCircleDist,origSizeX+2*inCage+numCircles*interCircleDist):
					if y < 0 or y >= origSizeY or x < 0 or x >= origSizeX or z >= origSizeZ:
						voxelyzeFile.write("0, ")
					
					elif swarmClimb and y > int(origSizeY/2):
						voxelyzeFile.write("0, ")

					else:
						voxelyzeFile.write(str(individual[netId("growthRate")].node["growthRate"]["state"][x,y,z])+", ")
						stringForMd5 += str(individual[netId("growthRate")].node["growthRate"]["state"][x,y,z])
						stringForMd5Control += str(individual[netId("growthRate")].node["growthRate"]["state"][x,y,z])
			voxelyzeFile.write("]]></Layer>\n")

		voxelyzeFile.write("</GrowthRate>\n")

	if "stiffness" in allNetworksOutputs:

		voxelyzeFile.write("<Stiffness>\n")
		voxelyzeFile.write("<MinElasticMod>"+str(minElasticMod)+"</MinElasticMod>\n")
		voxelyzeFile.write("<MaxElasticMod>"+str(maxElasticMod)+"</MaxElasticMod>\n")
		
		for z in range(origSizeZ+1*inCage):
			voxelyzeFile.write("<Layer><![CDATA[")
			# for y in range(origSizeY):
			# 	for x in range(origSizeX):
			for y in range(-2*inCage-numCircles*interCircleDist,origSizeY+2*inCage+numCircles*interCircleDist):

				if swarmClimb and y == int(origSizeY/2):
					for x in range(-2*inCage-numCircles*interCircleDist,origSizeX+2*inCage+numCircles*interCircleDist):
						voxelyzeFile.write("0, ")

				for x in range(-2*inCage-numCircles*interCircleDist,origSizeX+2*inCage+numCircles*interCircleDist):
					if y < 0 or y >= origSizeY or x < 0 or x >= origSizeX or z >= origSizeZ:
						voxelyzeFile.write("0, ")
					
					elif swarmClimb and y > int(origSizeY/2):
						voxelyzeFile.write("0, ")

					else:
						voxelyzeFile.write(str(remapStiffness(individual[netId("stiffness")].node["stiffness"]["state"][x,y,z]))+", ")
						stringForMd5 += str(individual[netId("stiffness")].node["stiffness"]["state"][x,y,z])
						#stringForMd5Control += str(individual[netId("stiffness")].node["stiffness"]["state"][x,y,z])
			voxelyzeFile.write("]]></Layer>\n")

		voxelyzeFile.write("</Stiffness>\n")


	if "frequency" in allNetworksOutputs:
		stringForMd5 += str(1.0/(individual[0].graph["frequency"]))
		stringForMd5Control += str(1.0/(individual[0].graph["frequency"]))


	voxelyzeFile.write("</Structure>\n\
</VXC>\n\
</VXA>")
	voxelyzeFile.close()

	m = hashlib.md5()
	m.update(stringForMd5)

	mShape = hashlib.md5()
	mShape.update(stringForMd5Shape)

	mControl = hashlib.md5()
	mControl.update(stringForMd5Control)
	

	# if np.min(individual[1*("materialMuscleType" in outputNodeNames[1])].node["materialMuscleType"]["state"])==0 and 0==np.max(individual[1*("materialMuscleType" in outputNodeNames[1])].node["materialMuscleType"]["state"]):
	# 	for edge in individual[1].in_edges(nbunch=["materialMuscleType"]):
	# 		print edge,individual[1].edge[edge[0]][edge[1]]["weight"]
	#print materialCounts, "\t", individual[0].graph["frequency"]
	
	return [materialCounts,m.hexdigest(),mShape.hexdigest(),mControl.hexdigest()]


def makeOneShapeOnly(individual):

	oldSize = np.sum(individual[netId("materialPresent")].node["materialPresent"]["state"]>0)

	if np.sum(individual[netId("materialPresent")].node["materialPresent"]["state"]>0) < 2:
		individual[netId("materialPresent")].node["materialPresent"]["oneShapeOnly"] = individual[netId("materialPresent")].node["materialPresent"]["state"]

	else:
		notYetChecked = []
		for z in range(origSizeZ):
			for y in range(origSizeY):
				for x in range(origSizeX):
					notYetChecked.append((x,y,z))

		individual[netId("materialPresent")].node["materialPresent"]["oneShapeOnly"] = np.zeros((origSizeX,origSizeY,origSizeZ))
		done = False
		largestShape = []
		queueToCheck = []
		while len(notYetChecked) > len(largestShape):
			queueToCheck.append(notYetChecked.pop(0))
			# queueToCheck.append(notYetChecked[0])
			thisShape = []
			if individual[netId("materialPresent")].node["materialPresent"]["state"][queueToCheck[0]] > 0:
				thisShape.append(queueToCheck[0])
			
			while len(queueToCheck) > 0:
				thisVoxel = queueToCheck.pop(0)
				x = thisVoxel[0]
				y = thisVoxel[1]
				z = thisVoxel[2]

				for neighborVoxel in [(x+1,y,z),(x-1,y,z),(x,y+1,z),(x,y-1,z),(x,y,z+1),(x,y,z-1)]:
					if neighborVoxel in notYetChecked:
						notYetChecked.remove(neighborVoxel)
						if individual[netId("materialPresent")].node["materialPresent"]["state"][neighborVoxel] > 0:
							queueToCheck.append(neighborVoxel)
							thisShape.append(neighborVoxel)

			if len(thisShape) > len(largestShape):
				largestShape = thisShape

		for loc in thisShape:
			individual[netId("materialPresent")].node["materialPresent"]["oneShapeOnly"][loc] = 1

	# if enforcingSolidBase:
	# 	# Find the first non-empty layer...	
	# 	for z in range(origSizeZ):
	# 		if np.sum(individual[1*("materialPresent" in outputNodeNames[1])].node["materialPresent"]["oneShapeOnly"][:,:,z]) != 0:
	# 			#print "*****************************"
	# 			#print(shapeMatrix[:,:,z])
	# 			individual[1*("materialPresent" in outputNodeNames[1])].node["materialPresent"]["oneShapeOnly"][:,:,z] = 3	
	# 			#print(shapeMatrix[:,:,z])
	# 			#print "*****************************"		
	# 			break
	# 			# and fill it in. What material does not really matter, nor the adaptive properties
	# 			# the part contacting with the ground does not change.
	# 			# Note: this works because later on we'll shift the robot so that it touches
	# 			# the ground. The base will touch.


def readSoftbotFitnessFile(filename="softbotsOutput.xml"):

	# Make sure that the file is there	
	i = 0
	MAX_ATTEMPTS = 60
	fileSize = 0

	while (i < MAX_ATTEMPTS) and (fileSize==0):

		try:
			fileSize = os.stat(filename).st_size
			thisFile = open(filename)
		except:
			fileSize = 0
		i+=1
		time.sleep(1) 

	if fileSize==0:
		print "ERROR: Cannot find a non-empty fitness file: abort"	
		exit(1)
	# --------------------------------------------------
	#  TO DO: refactor the following code avoiding replications
	fitness = 0
	dist = 0
	NumOutOfCageX = 0
	fitnessClimb = 0

	fitnessTagComposite = "<CompositeFitness>"
	# fitnessTagDist = "<Distance>"
	fitnessTagDist = "<DistX>"
	fitnessTagCage = "<NumOutOfCageX>"
	fitnessTagClimb = "<HighestVoxel>"
	fitnessTagVoxelNumber = "<VoxelNumber>"
	fitnessTagMinDistFromSource = "<MinDistanceFromSource" # incomplete tag in order to look at all possible tags of this kind

	tagRobotVolumeStart = "<RobotVolumeStart>"
	tagConvexHullVolumeStart = "<ConvexHullVolumeStart>"
	tagRobotVolumeEnd = "<RobotVolumeEnd>"
	tagConvexHullVolumeEnd = "<ConvexHullVolumeEnd>" 

	minDistanceFromSources = []
	voxelNumber = 0
	volumeStart = 0.0
	volumeEnd = 0.0
	chullVolumeStart = 0.0
	chullVolumeEnd = 0.0

	# fitnessTag = "<FinalCOM_DistX>"
	for line in thisFile:
		if fitnessTagComposite in line:
			fitness = abs(float(line[line.find(fitnessTagComposite)+len(fitnessTagComposite):line.find("</"+fitnessTagComposite[1:])]))
		if fitnessTagDist in line:
			dist = float(line[line.find(fitnessTagDist)+len(fitnessTagDist):line.find("</"+fitnessTagDist[1:])])
		if fitnessTagCage in line:
			NumOutOfCageX = float(line[line.find(fitnessTagCage)+len(fitnessTagCage):line.find("</"+fitnessTagCage[1:])])
		if fitnessTagClimb in line:
			fitnessClimb = float(line[line.find(fitnessTagClimb)+len(fitnessTagClimb):line.find("</"+fitnessTagClimb[1:])])
		if fitnessTagVoxelNumber in line:
			voxelNumber = float(line[line.find(fitnessTagVoxelNumber)+len(fitnessTagVoxelNumber):line.find("</"+fitnessTagVoxelNumber[1:])]) 			
		if tagRobotVolumeStart in line:
			volumeStart = float(line[line.find(tagRobotVolumeStart)+len(tagRobotVolumeStart):line.find("</"+tagRobotVolumeStart[1:])]) 			
		if tagRobotVolumeEnd in line:
			volumeEnd = float(line[line.find(tagRobotVolumeEnd)+len(tagRobotVolumeEnd):line.find("</"+tagRobotVolumeEnd[1:])]) 			
		if tagConvexHullVolumeStart in line:
			chullVolumeStart = float(line[line.find(tagConvexHullVolumeStart)+len(tagConvexHullVolumeStart):line.find("</"+tagConvexHullVolumeStart[1:])]) 			
		if tagConvexHullVolumeEnd in line:
			chullVolumeEnd = float(line[line.find(tagConvexHullVolumeEnd)+len(tagConvexHullVolumeEnd):line.find("</"+tagConvexHullVolumeEnd[1:])]) 			
		# REFACTORY WARNING: This is the only tag to be treated differently, as we are aggregating different similar tags, numbered
		if fitnessTagMinDistFromSource in line:
			minDistanceFromSources.append(float(line[line.find('>')+1:line.find('</')])) # a list is returned with min distances from all environmental sources

	if swarmClimb:
		return (fitness, fitnessClimb)

	if inCage:
		return (fitness, NumOutOfCageX)

	if sourcesEnabled:
		if (len(minDistanceFromSources) > 0 and voxelNumber != 0 and volumeStart != 0 and volumeEnd != 0 and chullVolumeStart != 0 and chullVolumeEnd != 0):
			return (minDistanceFromSources, voxelNumber, volumeStart, volumeEnd, chullVolumeStart, chullVolumeEnd)
		else:
			print("** ERROR **: Could not file some of the information we are looking for in file "+filename+". Aborting.")
			quit()

	return (fitness, dist)



def printName(var):
	print var+":",eval(var)


if __name__ == "__main__":
	mainTwoNetworks()

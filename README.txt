Introduction
-------------------------
This repository contains the code that allows to replicate the experiments presented in the following paper:

F. Corucci, N. Cheney, H. Lipson, C. Laschi, and J. Bongard, "Material properties affect evolution's ability to exploit morphological computation in growing soft-bodied creatures,"
ALIFE XV, The Fifteenth International Conference on the Synthesis and Simulation of Living Systems, 2016

If using this code for academic purposes, please cite the paper above.

For any question, feel free to contact: f( dot )corucci( at )sssup( dot )it

** DISCLAIMER **: This is experimental/research code: it was built on top of an experimental version of VoxCad that was not meant for public disclosure, extending it well beyond its original intended purpose. 
We are in the process of refactoring the whole setup, that will become more flexible and easier to use. Stay tuned!

Step 1: Installing VoxCad
-------------------------
(Install Qt and QMake if you have not already done so, specifically these packages: "libqt4-dev", "qt4-qmake", "libqwt-dev","freeglut3-dev","zlib1g-dev"
$ sudo apt-get install libqt4-dev qt4-qmake libqwt-dev freeglut3-dev zlib1g-dev
if you don't have them already)

$ cd [rootdir]/voxcad-code-2014
$ make
(if you get the error: "make: *** No rule to make target..." , run $qmake, then $make)
(NOTE: You can skip this step and install the Voxelyze library only for simulation without visualization) 

Step 2: Installing Voxelyze
---------------------------
1) install voxelyze library:
$ cd [rootdir]/voxcad-code-2014/Voxelyze
$ make                  # this will build the libraries
$ make installusr       # this will put them in ~/include so they can be accessed by voxelyzeMain  ( or use "sudo make installglobal" to put them in /lib )

2) make voxelyze executable:
$ cd [rootdir]/voxelyzeMain
$ make

Step 3: Installing qhull
-------------------------
For some advanced functions the program resorts to the qhull library. A precompiled version is provided, but might not work on your system. If it doesn't, it is recommended to compile it yourself.
1) Follow the instructions at http://www.qhull.org/ to download and compile the library (a 'make' in the qhull should be sufficient).
2) Once compiled, place the 'qhull' executable in [rootdir]/cppn folder
3) The 'qhull' executable should also be present in your binary path (try executing "qhull" from command line), if you intend to use this version of the GUI.

Step 4: Run the sample morphologies
----------------------------------------
Some sample morphologies are provided in the folder "[rootdir]/sample_morphologies".
1) In order to run them in VoxCad:
$ cd [rootdir]/voxcad-code-2014/release
$ ./VoxCad
-> File -> Import -> Simulation -> [rootdir]/sample_morphologies/4_sources_A.vxa
-> Physics Sandbox   (5th icon from the right)

2) In order to run them in voxelyze (no GUI):
$ cd [rootdir]/voxelyzeMain
$ ./voxelyze -f [rootdir]/sample_morphologies/4_sources_A.vxa -p   (-p flag = print simulation status to terminal, this flag is removed when called by HyperNEAT)

Step 5: Running Optimization
----------------------------
1) Install any dependecies required as you run python (an easy way to do so is using pip), examples of packages required are: scipy, numpy, and networkx
# sudo pip install networkx

2) make sure that the voxelyze executable and mainSoftbotTwoNetworks.py file are in the same path (by default they are in "[rootdir]/cppn", but you should perform runs with a copy of them in "[rootdir]/runs" for version control)
$ cd [rootdir]/cppn
if you have built the voxelzye executable, make sure you are using the desired version
$ cp [rootdir]/voxcad-code-2014/voxelyzeMain/voxelyze .

3) run the main file for the evolutionary algorithm, using the parameters you choose. Example:

python ./mainSoftbotTwoNetworks.py -n testName -r 0 --addSources R,L --saveVxaEvery 1

This will run a test named 'testName' with a random seed 0, adding two lateral light sources, one on the left (L), one on the right (R).
It will also save the whole population every generation, in the form of several .vxa files.
There are several others command-line options you can play with, defined at the beginning of the file "mainSoftbotTwoNetworks.py".

** IMPORTANT **: be sure to add at least a light source (with the --addSources argument), as this enables the code specific to growth (this version might not work properly when no sources are added to the environment).

Step 6: Analyzing Data
-----------------------
1) To see how the fitness of the best individual so far changes over each generation of optimization, refer to the file "[runDir]/bestSoFar/bestOfGen.txt" 
2) To find each of the .vxa files which were once the top performing softbot, see the folder "[runDir]/bestSoFar/fitOnly"
3) To see a summary of each individual (grouped by generation), see the folder "[runDir]/allIndividualsData"

For specific requests to analyze and plot various metrics, feel free to contact me.
